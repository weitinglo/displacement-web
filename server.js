const express = require('express')
var bodyParser = require('body-parser');
const app = express()
const port = 3000
const sqlite3 = require('sqlite3');

let db = new sqlite3.Database('displacementDB.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the in-memory SQlite database.');
});

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/view'));
app.use(express.static(__dirname + '/node_modules'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/view/" + "index.html");
});

app.get('/base', function (req, res) {
    let name = req.query.line;
    let hour = req.query.hour;
    var jsonRes = {
        code: 200,
        data: ""
    };
    
   // sqlQuery = "SELECT * FROM BASE WHERE NAME ='"+name+"' AND TIME = '" +hour+"'";

    sqlQuery = "SELECT * FROM BASE WHERE NAME  ='"+name+"' AND TIME = '" +hour+"' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE NAME  ='"+name+"' AND TIME = '" +hour+"')";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/base_whole', function (req, res) {
    let hour = req.query.hour;
    var jsonRes = {
        code: 200,
        data: ""
    };
    
    sqlQuery = "SELECT * FROM BASE WHERE TIME = '" +hour+"' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE TIME = '" +hour+"')";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/compare', function (req, res) {
    let name = req.query.line;
    let hour = req.query.hour;
    let date = req.query.date;
    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT * FROM COMPARE WHERE NAME  ='"+name+"' AND TIME = '" +hour+"' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE NAME  ='"+name+"' AND TIME = '" +hour+"') AND DATE ='"+date+"'";

   // sqlQuery = "SELECT * FROM COMPARE WHERE NAME ='"+name+"' AND TIME = '" +hour+"' AND DATE ='"+date+"'";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/compare_section', function (req, res) {
    let section = req.query.section;
    let name = req.query.name;
    let hour = req.query.hour;
    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT * FROM COMPARE WHERE SECTION  ='"+section+"' AND NAME = '" +name +"' AND TIME ='"+hour+"' order by date";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/compare_section_hour', function (req, res) {
    let section = req.query.section;
    let name = req.query.name;
    let hour = req.query.hour;
    let date = req.query.date;
    let dateFormate = date.substring(0,4)+"/"+date.substring(4,6)+"/"+date.substring(6,8);
    let preDate = new Date(dateFormate);
    preDate.setDate(preDate.getDate() - 1);
    let preSearchDate = preDate.getFullYear() + ('0' + (preDate.getMonth() + 1)).slice(-2) + ('0' + preDate.getDate()).slice(-2);
    let curDate = new Date(dateFormate);
    let curSearchDate = curDate.getFullYear() + ('0' + (curDate.getMonth() + 1)).slice(-2) + ('0' + curDate.getDate()).slice(-2);

    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT * FROM COMPARE WHERE ((DATE = '"+preSearchDate+"' AND TIME >="+hour +") OR (DATE='"+curSearchDate+"' AND TIME < "+hour + ")) AND SECTION  ='"+section+"' AND NAME = '" +name + "'";
  //  console.log(sqlQuery)
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});


app.get('/compare_whole', function (req, res) {
    let hour = req.query.hour;
    let date = req.query.date;
    var jsonRes = {
        code: 200,
        data: ""
    };
  //  sqlQuery = "SELECT * FROM COMPARE WHERE TIME = '" +hour+"' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE TIME = '" +hour+"') AND DATE ='"+date+"'";
    sqlQuery = "SELECT * FROM COMPARE WHERE TIME = '" +hour+"' AND DATE ='"+date+"'";

    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/home_behavior', function (req, res) {
    let name = req.query.line;
    let hour = req.query.hour;
    let date = req.query.date;
    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT DISPLACEMENT,DIRECTION, SECTION FROM COMPARE WHERE NAME  ='"+name+"' AND TIME = '" +hour+"' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE NAME  ='"+name+"' AND TIME = '" +hour+"') AND DATE ='"+date+"'";

    //sqlQuery = "SELECT * FROM COMPARE WHERE NAME ='"+name+"' AND TIME = '" +hour+"' AND DATE ='"+date+"'";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});


app.post('/save_filter', function (req, res) {
    let name = req.body.name;
    let section = req.body.section;
    let resolution = req.body.resolution;
    let time = req.body.time;
    var jsonMsg = {
        code: 200,
        data: ""
    };
    
    sqlQuery = "INSERT INTO FILTER (NAME,SECTION, RESOLUTION, TIME) VALUES ($name,$section,$resolution,$time)";
    db.run(sqlQuery, {
        $name: name,
        $section: section,
        $resolution: resolution,
        $time: time
    }, function (err) {
        if (err) {
            jsonMsg.msg = err.message;
            jsonMsg.code = 500;
            res.send(jsonMsg);
        } else {
            jsonMsg.msg = "ok";
            jsonMsg.code = 200;
            res.send(jsonMsg);
        }

    });
});


app.get('/floating', function (req, res) {
  
    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT DISPLACEMENT ,X,Y,Z FROM BASE WHERE NAME LIKE 'A%' AND SECTION NOT IN (SELECT SECTION FROM FILTER WHERE NAME LIKE 'A%')";

    //sqlQuery = "SELECT * FROM COMPARE WHERE NAME ='"+name+"' AND TIME = '" +hour+"' AND DATE ='"+date+"'";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/section', function (req, res) {
    let section = req.query.section;

    var jsonRes = {
        code: 200,
        data: ""
    };
    sqlQuery = "SELECT * FROM COMPARE WHERE SECTION ="+section+" ORDER BY DATE";
    db.all(sqlQuery, [], (err, rows) => {
        if (!err) {
            jsonRes.data = rows;
            res.send(jsonRes);
        }
    });
});

app.get('/displacememt', function (req, res) {
    let sqlQuery = "SELECT displacement, base_x, base_y, base_z FROM COMPARE WHERE date='20201010'";
    db.all(sqlQuery, [], (err, rows) => {
        if (err) {
            let jsonMsg = {
                msg: err.message,
                code: 500,
            }
            res.send(jsonMsg);
        } else {
            res.send(rows);
        }
    });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))