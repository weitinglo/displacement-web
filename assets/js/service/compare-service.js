
app.factory("compareService", function ($http) {
    return {
        getSection: function (section, hour) {
            return $http.get('section?section=' + section + '&hour=' + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCompareWhole: function ( hour, date) {
            return $http.get('compare_whole?hour=' + hour + '&date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
    }

})
