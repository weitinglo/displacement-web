
app.factory("displacementService", function ($http) {
    return {
        getBase: function (line, hour) {
            return $http.get('base?line=' + line + '&hour=' + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getBaseWhole: function (hour) {
            return $http.get('base_whole?hour=' + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getFloating: function () {
            return $http.get('floating')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCompare: function (line, hour, date) {
            return $http.get('compare?line=' + line + '&hour=' + hour + '&date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCompareSection: function (section,name, hour) {
            return $http.get('compare_section?section=' + section + '&name=' + name  + '&hour=' + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCompareSectionHour: function (section,name, hour,date) {
            return $http.get('compare_section_hour?section=' + section + '&name=' + name + '&hour=' + hour+ '&date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCompareWhole: function ( hour, date) {
            return $http.get('compare_whole?hour=' + hour + '&date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getHomeBehavior: function (line, hour, date) {
            return $http.get('home_behavior?line=' + line + '&hour=' + hour + '&date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        saveFilter: function (name, section, resolution,time) {
            return $http.post('save_filter', {
                'name': name,
                'section': section,
                'resolution':resolution,
                'time':time
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }

})
