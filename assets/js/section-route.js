app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider
      .when('/section', {
        templateUrl: '/section.htm',
        controller: 'sectionController'
      }) 
      .otherwise({
        redirectTo: '/section'
      });
  }]);