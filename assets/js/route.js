app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: '/home.htm',
        controller: 'homeController'
      })
      .when('/base', {
        templateUrl: '/base.htm',
        controller: 'baseController'
      })
      .when('/live_compare', {
        templateUrl: '/live_compare.htm',
        controller: 'liveCompareController'
      })
      .when('/cloud', {
        templateUrl: '/cloud.htm',
        controller: 'cloudController'
      })
      .when('/history', {
        templateUrl: '/history.htm',
        controller: 'historyController'
      })
      .when('/start', {
        templateUrl: '/start.htm',
        controller: 'startController'
      })
      .otherwise({
        redirectTo: '/home'
      });
  }]);