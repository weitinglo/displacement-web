app.directive("sideMenu", function () {
  return {
    template:
      '<div class="sidebar" data-color="orange">' +
      '<div class="logo text-center">' +

      '<a href="http://www.creative-tim.com" class="side-menu-text">' +
      '變位監測' +
      '</a>' +
      '</div>' +
      '<div class="sidebar-wrapper" id="sidebar-wrapper">' +
      '<ul class="nav">' +
      '<li id="homeMenu">' +
        '<a href="#!home">' +
        '<i class="far fa-sun"></i>' +
        '<p>累積變位量</p>' +
        '</a>' +
      ' </li>' +
      '<li id="liveCompareMenu">' +
        '<a href="#!live_compare">' +
        '<i class="fas fa-history"></i>' +
        '<p>即時變位量(1Hr)</p>' +
        '</a>' +
      ' </li>' +
      '<li id="historyMenu">' +
        '<a href="#!history">' +
        '<i class="far fa-calendar-alt"></i>' +
        '<p>歷史數據</p>' +
        '</a>' +
      '</li>' +
      // '<li id="baseMenu">' +
      //   '<a href="#!base">' +
      //   '<i class="now-ui-icons design_app design-2_ruler-pencil"></i>' +
      //   '<p>起始數據</p>' +
      //   '</a>' +
      // '</li>' +
      //  '<li id="cloudMenu">'+
      //     '<a href="#!cloud">'+
      //       '<i class="fas fa-cloud"></i>'+
      //       '<p>起始點雲</p>'+
      //     '</a>'+
      //  ' </li>'+
      // '<li id="dailyMenu" >'+
      //   '<a href="#!daily">'+
      //     '<i class="now-ui-icons business_chart-bar-32 side-menu-icon"></i>'+
      //     '<p>每日變化</p>'+
      //   '</a>'+
      // '</li>'+
      // '<li id="startMenu">' +
      // '<a href="#!start">' +
      // '<i class="fas fa-cube"></i>' +
      // '<p>起始線段</p>' +
      '</a>' +
      '</li>' +
      '</ul>' +
      '</div>' +
      '</div>'
  };
});