app.directive("lotFooter", function() {
    return {
        template : 
        '<footer class="footer">'+
       ' <div class=" container-fluid ">'+
         ' <nav>'+
            '<ul>'+
              '<li>'+
                '<a href="http://www.chiper.com.tw/">'+
                 '  巨普科技'+
                '</a>'+
             ' </li>'+
              '<li>'+
                '<a href="http://www.chiper.com.tw/#!/about">'+
                  '關於我們'+
                '</a>'+
              '</li>'+
              
            '</ul>'+
         ' </nav>'+
          '<div class="copyright" id="copyright">'+
            '&copy; 2020, Designed by  <a href="http://www.lot.com.tw/">巨普科技</a>'+
          '</div>'+
        '</div>'+
      '</footer>'
    };
});