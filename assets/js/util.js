function updateMenu(menu) {
  $("#homeMenu").removeClass("active");
  $("#historyMenu").removeClass("active");
  $("#baseMenu").removeClass("active");
  $("#cloudMenu").removeClass("active");
  $("#startMenu").removeClass("active");
  $("#liveCompareMenu").removeClass("active");

  switch (menu) {
    case 'home':
      $("#homeMenu").addClass("active");
      break;
    case 'base':
      $("#baseMenu").addClass("active");
      break;
    case 'cloud':
      $("#cloudMenu").addClass("active");
      break;
    case 'history':
      $("#historyMenu").addClass("active");
      break;
    case 'start':
      $("#startMenu").addClass("active");
      break;
    case 'live_compare':
      $("#liveCompareMenu").addClass("active");
      break;
  }
}

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};
var chiperUtils = {
  misc: {
    navbar_menu_visible: 0
  },

  showNotification: function (from, align, msg) {
    color = 'info';
    $.notify({
      icon: "fa fa-bell",
      message: msg

    }, {
      type: color,
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  },
  showErrorNotification: function (from, align, msg) {
    color = 'danger';
    $.notify({
      icon: "fa fa-exclamation-triangle",
      message: msg

    }, {
      type: color,
      timer: 5000,
      placement: {
        from: from,
        align: align
      }
    });
  },
  showSuccessNotification: function (from, align, msg) {
    color = 'success';
    $.notify({
      icon: "fa fa-check-circle",
      message: msg

    }, {
      type: color,
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }


};
