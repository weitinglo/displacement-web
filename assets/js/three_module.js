import * as THREE from '../build/three.module.js';
import {TrackballControls} from '../jsm/controls/TrackballControls.js';
import {OrbitControls} from '../jsm/controls/OrbitControls.js';
import {OBJLoader2} from '../jsm/loaders/OBJLoader2.js';
import {MTLLoader} from '../jsm/loaders/MTLLoader.js';
import {MtlObjBridge} from '../jsm/loaders/obj2/bridge/MtlObjBridge.js';
