
import * as THREE from '../../build/three.module.js';
import { TrackballControls } from '../../jsm/controls/TrackballControls.js';
import { OBJLoader2 } from '../../jsm/loaders/OBJLoader2.js';
import { MTLLoader } from '../../jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from '../../jsm/loaders/obj2/bridge/MtlObjBridge.js';

app.controller('startController', function ($scope, displacementService) {
    updateMenu('start');
    var camera, controls, scene, renderer;
    var mouse = new THREE.Vector2();
    var raycaster = new THREE.Raycaster();
    const manager = new THREE.LoadingManager();
    $scope.loadingMsg = "資料讀取中請稍後";
    $scope.loading=false;
    $scope.selectedHour = 0;
    $scope.hours = [];
    $scope.baseData = [];


    $scope.getBaseWhole = function (hour) {
        $scope.loading = true;
        displacementService.getBaseWhole(hour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                drawCoords(data.data);
                $scope.baseData = data.data;
            }
        });
    }

    $scope.setHours = function () {
        for (let i = 0; i < 24; i++) {
            $scope.hours.push(i);
        }
    }

    $scope.search = function () {
        if ($scope.selectedHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧小時");
            return;
        }
        $scope.threeJSinit();
        $scope.getBaseWhole($scope.selectedHour);
    }


 


    /*************Three JS******************/

    function drawCoords(coords_array) {
        for (var i = 0; i < coords_array.length; i++) {
            
            let color = "rgb(29, 149, 255)";
            const material = new THREE.MeshPhongMaterial({
                color: color
            });
            const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
            const cube = new THREE.Mesh(geometry, material);
            cube.position.set(parseFloat(coords_array[i].x), parseFloat(coords_array[i].y), parseFloat(coords_array[i].z));
            scene.add(cube);
            
            
        }
        controls.update();
        render();
    }

    function animate() {
        requestAnimationFrame(animate);
        controls.update();
    }

    function render() {
        renderer.render(scene, camera);
    }
    manager.onStart = function () {
        $scope.loadingMsg = "3D 檔讀取中";
        $scope.$apply();
    };
    
    manager.onProgress = function () {
        $scope.loadingMsg = "3D 建模中";
        $scope.$apply();
    };
    manager.onLoad = function ( ) {
        $scope.loading=false;
        $scope.$apply()
    };
    function draw_house_model(model_name) {
        var mtlLoader = new MTLLoader();
        mtlLoader.load('../chiper_model/' + model_name + '.mtl', (mtlParseResult) => {
            const materials = MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);
            var objLoader = new OBJLoader2(manager);
            objLoader.addMaterials(materials);
            objLoader.load('../chiper_model/' + model_name + '.obj', (root) => {
                scene.add(root);
                root.position.set(0, -0.1, -0.15);
                render();
            });
        });
    }
    function put_instruction() {
        var text2 = document.createElement('div');
        text2.style.position = 'absolute';
        text2.style.width = 100;
        text2.style.height = 100;
        text2.style.color = "white";
        text2.innerHTML = "【操作說明】<br>滑鼠左鍵：拖曳<br>滑鼠右鍵：平移<br>滑鼠滾輪：前後移動";
        text2.style.top = 30 + 'px';
        text2.style.left = 35 + 'px';
        document.getElementById('home_canvas').appendChild(text2);
    }


    let INTERSECTED;
    function mouseOver(event) {
        try{
            if(window.scrollY!=0)
                scrollWin();
            const menu_WIDTH = 275;
            const HEIGHT = 535;
            mouse.x = ((event.clientX-275) / (window.innerWidth - menu_WIDTH)) * 2 - 1;
            mouse.y = - ((event.clientY-38) / HEIGHT) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);
            let intersects = raycaster.intersectObjects( scene.children );
            
            if (intersects.length < 1) return;
            if ( intersects.length > 0 ) {
                if ( INTERSECTED != intersects[0].object ) {
                    if ( INTERSECTED && INTERSECTED.material && INTERSECTED.material.emissive ){
                        INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
                    }
                    INTERSECTED = intersects[ 0 ].object;
                    INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
                    INTERSECTED.material.emissive.setHex( 0xff0000 );
                }
            } else {
                if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
                INTERSECTED = null;
            }
            renderer.render( scene, camera );
        }catch(err){
            console.log(err)
        }
        
    }

    $scope.threeJSinit = function () {
        const menu_WIDTH = 275;
        const HEIGHT = 535;
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x000000);
        camera = new THREE.PerspectiveCamera(20, (window.innerWidth - menu_WIDTH) / HEIGHT, 1, 9000);
        camera.position.x = 73;
        camera.position.y = 15;
        camera.position.z = 35;
        camera.up.set(0, 0, 1);
        scene.add(camera);
        var light = new THREE.DirectionalLight(0xFFFFFF, 1.2, 1000); // white, intensity=1, distance = 500
        light.position.set(10, 0, 25); // (x, y, z)
        scene.add(light);
        const axesHelper = new THREE.AxesHelper(5);
        scene.add(axesHelper);

        var models = ['Tile_+002_+002_+000', 'Tile_+003_+001_+000', 'Tile_+003_+002_+000']
        models.forEach(element => draw_house_model(element));
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor("#e5e5e5");
        renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
        renderer.domElement.addEventListener("mousemove", mouseOver);

        const the_canvas = document.getElementById('home_canvas');
        the_canvas.innerHTML = "";
        the_canvas.appendChild(renderer.domElement);
        put_instruction();

        window.addEventListener('resize', () => {
            renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
            camera.aspect = (window.innerWidth - menu_WIDTH) / HEIGHT;

            camera.updateProjectionMatrix();
        })

        controls = new TrackballControls(camera, the_canvas);
        controls.rotateSpeed = 2;
        controls.zoomSpeed = 5.0;
        controls.noZoom = false;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        controls.target = new THREE.Vector3(40,-18,15)
        render();
        controls.addEventListener('change', render);
        controls.update();
        animate();
    }

    function scrollWin() {
        window.scrollTo(0, 0);
      }


    /*******************Done Three JS**************/
    $scope.init = function () {
        $scope.setHours();
      //  $scope.search();
       // $scope.threeJSinit();
       
    }
    $scope.init();
});

