
import * as THREE from '../../build/three.module.js';
import { TrackballControls } from '../../jsm/controls/TrackballControls.js';
import { OBJLoader2 } from '../../jsm/loaders/OBJLoader2.js';
import { MTLLoader } from '../../jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from '../../jsm/loaders/obj2/bridge/MtlObjBridge.js';

app.controller('homeController', function ($scope, displacementService) {
    updateMenu('home');
    var camera, controls, scene, renderer;
    var objId = 0;
    $scope.selectedHour = 0;
    $scope.selectedDate = null;
    $scope.hours = [];
    $scope.preLine = 0;
    //$scope.lineType = ['AH1', 'AH2', 'AH3', 'AH4', 'AH5', 'AH6', 'AH7', 'AH8', 'AW1', 'AW2', 'AW3', 'BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6', 'BH7', 'BH8', 'BH9', 'BW1', 'BW2'];
    $scope.lineType = ['AH1', 'AH2', 'AH3', 'AH4', 'AH5', 'AH6', 'AH7', 'AW1', 'AW2', 'AW3', 'BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6', 'BH7', 'BH8', 'BH9', 'BW1', 'BW2'];

    $scope.compareData = {};
    $scope.compareData['AH1'] = { data: [], name: "設備A-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartAH1", showChart: false };
    $scope.compareData['AH2'] = { data: [], name: "設備A-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartAH2", showChart: false };
    $scope.compareData['AH3'] = { data: [], name: "設備A-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartAH3", showChart: false };
    $scope.compareData['AH4'] = { data: [], name: "設備A-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartAH4", showChart: false };
    $scope.compareData['AH5'] = { data: [], name: "設備A-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartAH5", showChart: false };
    $scope.compareData['AH6'] = { data: [], name: "設備A-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartAH6", showChart: false };
    $scope.compareData['AH7'] = { data: [], name: "設備A-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartAH7", showChart: false };
    $scope.compareData['AH8'] = { data: [], name: "設備A-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartAH8", showChart: false };
    $scope.compareData['AW1'] = { data: [], name: "設備A-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartAW1", showChart: false };
    $scope.compareData['AW2'] = { data: [], name: "設備A-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartAW2", showChart: false };
    $scope.compareData['AW3'] = { data: [], name: "設備A-圍牆線 3", chart: "顯示圍牆線 3 表格", chartId: "chartAW3", showChart: false };

    $scope.compareData['BH1'] = { data: [], name: "設備B-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartBH1", showChart: false };
    $scope.compareData['BH2'] = { data: [], name: "設備B-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartBH2", showChart: false };
    $scope.compareData['BH3'] = { data: [], name: "設備B-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartBH3", showChart: false };
    $scope.compareData['BH4'] = { data: [], name: "設備B-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartBH4", showChart: false };
    $scope.compareData['BH5'] = { data: [], name: "設備B-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartBH5", showChart: false };
    $scope.compareData['BH6'] = { data: [], name: "設備B-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartBH6", showChart: false };
    $scope.compareData['BH7'] = { data: [], name: "設備B-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartBH7", showChart: false };
    $scope.compareData['BH8'] = { data: [], name: "設備B-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartBH8", showChart: false };
    $scope.compareData['BH9'] = { data: [], name: "設備B-房子線 9", chart: "顯示房子線 9 表格", chartId: "chartBH9", showChart: false };
    $scope.compareData['BW1'] = { data: [], name: "設備B-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartBW1", showChart: false };
    $scope.compareData['BW2'] = { data: [], name: "設備B-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartBW2", showChart: false };

    $scope.maxMin = {};
    for (let i in $scope.lineType) {
        $scope.maxMin[$scope.lineType[i]] = { max: "", min: "" };
    }


    var chartAW1 = null;
    var chartAW2 = null;
    var chartAW3 = null;
    var chartAH1 = null;
    var chartAH2 = null;
    var chartAH3 = null;
    var chartAH4 = null;
    var chartAH5 = null;
    var chartAH6 = null;
    var chartAH7 = null;
    var chartAH8 = null;

    var chartBW1 = null;
    var chartBW2 = null;
    var chartBH1 = null;
    var chartBH2 = null;
    var chartBH3 = null;
    var chartBH4 = null;
    var chartBH5 = null;
    var chartBH6 = null;
    var chartBH7 = null;
    var chartBH8 = null;
    var chartBH9 = null;

    var filter = false;




    $scope.getCompare = function (hour, date, line) {
        displacementService.getCompare(line, hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
              //  drawCoords(data.data);
                $scope.findMaxMin(data.data, line);
                $scope.compareData[line].data = data.data;
                if (filter == true) {
                    for (i in data.data) {
                        if (Math.abs(data.data[i].displacement) > 3) {
                            displacementService.saveFilter(data.data[i].name, data.data[i].section, 0.1, 0);
                        }
                    }
                }
            }
        });
    }

    $scope.findMaxMin = function (data, type) {
        let min = 100;
        let max = -100;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].displacement;
            if (value > max) {
                max = value;
            } else if (value < min) {
                min = value;
            }
        }
        $scope.maxMin[type].max = max;
        $scope.maxMin[type].min = min;

    }
    $scope.setHours = function () {
        for (let i = 0; i < 1; i++) {
            $scope.hours.push(i);
        }
    }
    $scope.getSearchDateFormat = function (d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }
    $scope.search = function () {
        if ($scope.selectedHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧小時")
        }
        if ($scope.selectedDate == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧日期")
        }
     //   removeEntity('current_mesh');
        objId = 0;
        for (let i in $scope.lineType) {
            $scope.getCompare($scope.selectedHour, $scope.getSearchDateFormat($scope.selectedDate), $scope.lineType[i]);

        }
    }

    $scope.plotAW = function (chartTitle, line) {
        $scope.preLine = line;
        let chartData = [];
        var ctx = "";
        chartData = $scope.compareData[line].data;
        ctx = document.getElementById($scope.compareData[line].chartId).getContext('2d');
        $scope.compareData[line].showChart = !$scope.compareData[line].showChart;
        $scope.chartData = [];
        $scope.label = [];
        let len = chartData.length;
        let lastSection = chartData[len - 1].section
        $scope.chartData = new Array(lastSection);
        for (let i = 0; i < len; i++) {
            let section = chartData[i].section;
            $scope.chartData[section] = chartData[i].displacement;
        }
        for (let i = 0; i < lastSection; i++) {
            $scope.label.push(i * 10);
        }
        let options = {
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "區域線段（公分）",
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "變位量（公分）",
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if(label = '設備A-房子線 1'){
                            console.log(tooltipItem)
                        }
                        console.log( data.datasets[tooltipItem.datasetIndex])
                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        return label;
                    }
                }
            },
            onClick: function (evt, item) {
                console.log(item)
                if (item.length > 0) {
                    window.open("/section.html?section=" + item[0]._model.label);
                }
            },
        }
        let chartDataSetting = {
            labels: $scope.label, datasets: [{ label: chartTitle, data: $scope.chartData, backgroundColor: 'red', borderWidth: 1 }]
        }
        if (line == 'AH1') {
            chartAH1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH1.update();
        } else if (line == 'AH2') {
            chartAH2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH2.update();
        } else if (line == 'AH3') {
            chartAH3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH3.update();
        } else if (line == 'AH4') {
            chartAH4 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH4.update();
        } else if (line == 'AH5') {
            chartAH5 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH5.update();
        } else if (line == 'AH6') {
            chartAH6 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH6.update();
        } else if (line == 'AH7') {
            chartAH7 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH7.update();
        } else if (line == 'AH8') {
            chartAH8 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH8.update();
        } else if (line == 'AW1') {
            chartAW1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW1.update();
        } else if (line == 'AW2') {
            chartAW2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW2.update();
        } else if (line == 'AW3') {
            chartAW3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW3.update();
        } else if (line == 'BH1') {
            chartBH1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH1.update();
        } else if (line == 'BH2') {
            chartBH2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH2.update();
        } else if (line == 'BH3') {
            chartBH3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH3.update();
        } else if (line == 'BH4') {
            chartBH4 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH4.update();
        } else if (line == 'BH5') {
            chartBH5 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH5.update();
        } else if (line == 'BH6') {
            chartBH6 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH6.update();
        } else if (line == 'BH7') {
            chartBH7 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH7.update();
        } else if (line == 'BH8') {
            chartBH8 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH8.update();
        } else if (line == 'BH9') {
            chartBH9 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH9.update();
        } else if (line == 'BW1') {
            chartBW1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBW1.update();
        } else if (line == 'BW2') {
            chartBW2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBW2.update();
        }
    }

    /*Three JS*/
    function get_gradient_color(coord_z) {
        if (coord_z > 1 & coord_z <= 3) {
            return [0, 1, 0];

        } else if (coord_z > 3 & coord_z <= 5) {
            return [1, 1, 0];
        } else if (coord_z > 5) {
            return [1, 0, 0];
        } else {
            return [0, 0, 0];
        }
    }
    $scope.positions=[];
    let pos1 ={
        xPos:-2.2,
        yPos:-14.4,
        zPos:-0.06,
        xRot:1.5,
        yRot:-0.4
    }
    $scope.positions.push(pos1);
    function addPlanes(jsonVal){
        const geometry = new THREE.PlaneGeometry( 1, 1, 1 );
        const material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
        const plane = new THREE.Mesh( geometry, material );
        plane.position.x = -2.2;
        plane.position.y = -14.4;
        plane.position.z = -0.06;
        plane.rotation.y = -0.4
        plane.rotation.x = 1.5
        scene.add( plane );
    }
    function drawCoords(coords_array) {
        var position = [];
        var color = [];

        for (var i = 0; i < coords_array.length; i++) {
            position.push(parseFloat(coords_array[i].base_x));
            position.push(parseFloat(coords_array[i].base_y));
            position.push(parseFloat(coords_array[i].base_z));

            const coord_color = get_gradient_color(coords_array[i].displacement);
            color.push(coord_color[0], coord_color[1], coord_color[2]);
        }


        

        // build geometry
        var geometry = new THREE.BufferGeometry();
        if (position.length > 0) geometry.setAttribute('position', new THREE.Float32BufferAttribute(position, 3));
        if (color.length > 0) geometry.setAttribute('color', new THREE.Float32BufferAttribute(color, 3));
        geometry.computeBoundingSphere();

        // build material
        var material = new THREE.PointsMaterial({ size: 2 });
        if (color.length > 0) {
            material.vertexColors = true;
        } else {
            material.color.setHex(Math.random() * 0xffffff);
        }

        // build point cloud

        var mesh = new THREE.Points(geometry, material);
        mesh.name = 'current_mesh' + objId;
        objId++;
        scene.add(mesh);
        const center = mesh.geometry.boundingSphere.center;
        controls.target.set(center.x, center.y, center.z);
        controls.update();
        render();
    }
    function animate() {
        requestAnimationFrame(animate);
        controls.update();
    }

    function render() {
        renderer.render(scene, camera);
    }
    function draw_house_model(model_name) {
        var mtlLoader = new MTLLoader();
        mtlLoader.load('../chiper_model/' + model_name + '.mtl', (mtlParseResult) => {
            const materials = MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);
            var objLoader = new OBJLoader2();
            objLoader.addMaterials(materials);

            objLoader.load('../chiper_model/' + model_name + '.obj', (root) => {
                scene.add(root);
                root.position.set(0, -0.1, -0.15);
                render();
            });
        });
    }
    function put_instruction() {
        var text2 = document.createElement('div');
        text2.style.position = 'absolute';
        text2.style.width = 100;
        text2.style.height = 100;
        text2.style.color = "white";
        text2.innerHTML = "【操作說明】<br>滑鼠左鍵：拖曳<br>滑鼠滾輪：前後移動";
        text2.style.top = 30 + 'px';
        text2.style.left = 35 + 'px';
        document.getElementById('home_canvas').appendChild(text2);
    }
    $scope.threeJSinit = function () {
        const menu_WIDTH = 300;
        const HEIGHT = 500;

        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x000000);

        camera = new THREE.PerspectiveCamera(20, (window.innerWidth - menu_WIDTH) / HEIGHT, 0.01, 9000);
        camera.position.x = 100;
        camera.position.y = -10;
        camera.position.z = 30;
        camera.up.set(0, 0, 1);
        scene.add(camera);

        var light = new THREE.DirectionalLight(0xFFFFFF, 1, 1000); // white, intensity=1, distance = 500
        light.position.set(10, 0, 25); // (x, y, z)
        scene.add(light);
        const axesHelper = new THREE.AxesHelper(5);
        scene.add(axesHelper);

        var models = ['Tile_+002_+002_+000', 'Tile_+003_+001_+000', 'Tile_+003_+002_+000']
        models.forEach(element => draw_house_model(element));
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor("#e5e5e5");
        renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);

        const the_canvas = document.getElementById('home_canvas');
        the_canvas.appendChild(renderer.domElement);
        put_instruction();

        window.addEventListener('resize', () => {
            renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
            camera.aspect = (window.innerWidth - menu_WIDTH) / HEIGHT;

            camera.updateProjectionMatrix();
        })

        controls = new TrackballControls(camera, the_canvas);
        controls.rotateSpeed = 2;
        controls.zoomSpeed = 5.0;
        controls.noZoom = false;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        render();
        controls.addEventListener('change', render);
        controls.update();
    }
    $scope.init = function () {
        $scope.setHours();
        $scope.selectedDate = new Date('2020-10-12');
        $scope.search();
        $scope.threeJSinit();
        animate();
        addPlanes('')
    }
    $scope.init();
});

