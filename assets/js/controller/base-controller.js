
app.controller('baseController', function ($scope, displacementService) {
    updateMenu('base');
    $scope.hours =[];
    $scope.selectedHour = 0;
    $scope.lineType = ['AH1', 'AH2', 'AH3', 'AH4', 'AH5', 'AH6', 'AH7', 'AH8', 'AW1', 'AW2', 'AW3', 'AW4', 'AW5', 'AW6', 'BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6', 'BH7', 'BH8', 'BH9', 'BW1', 'BW2','BW3', 'BW4'];
    $scope.baseData = {};
    $scope.baseData['AH1'] = { data: [], name: "設備A-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartAH1", showChart: false };
    $scope.baseData['AH2'] = { data: [], name: "設備A-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartAH2", showChart: false };
    $scope.baseData['AH3'] = { data: [], name: "設備A-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartAH3", showChart: false };
    $scope.baseData['AH4'] = { data: [], name: "設備A-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartAH4", showChart: false };
    $scope.baseData['AH5'] = { data: [], name: "設備A-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartAH5", showChart: false };
    $scope.baseData['AH6'] = { data: [], name: "設備A-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartAH6", showChart: false };
    $scope.baseData['AH7'] = { data: [], name: "設備A-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartAH7", showChart: false };
    $scope.baseData['AH8'] = { data: [], name: "設備A-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartAH8", showChart: false };
    $scope.baseData['AW1'] = { data: [], name: "設備A-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartAW1", showChart: false };
    $scope.baseData['AW2'] = { data: [], name: "設備A-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartAW2", showChart: false };
    $scope.baseData['AW3'] = { data: [], name: "設備A-圍牆線 3", chart: "顯示圍牆線 3 表格", chartId: "chartAW3", showChart: false };
    $scope.baseData['AW4'] = { data: [], name: "設備A-圍牆線 4", chart: "顯示房子線 1 表格", chartId: "chartAW4", showChart: false };
    $scope.baseData['AW5'] = { data: [], name: "設備A-圍牆線 5", chart: "顯示圍牆線 2 表格", chartId: "chartAW5", showChart: false };
    $scope.baseData['AW6'] = { data: [], name: "設備A-圍牆線 6", chart: "顯示圍牆線 3 表格", chartId: "chartAW6", showChart: false };

    $scope.baseData['BH1'] = { data: [], name: "設備B-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartBH1", showChart: false };
    $scope.baseData['BH2'] = { data: [], name: "設備B-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartBH2", showChart: false };
    $scope.baseData['BH3'] = { data: [], name: "設備B-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartBH3", showChart: false };
    $scope.baseData['BH4'] = { data: [], name: "設備B-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartBH4", showChart: false };
    $scope.baseData['BH5'] = { data: [], name: "設備B-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartBH5", showChart: false };
    $scope.baseData['BH6'] = { data: [], name: "設備B-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartBH6", showChart: false };
    $scope.baseData['BH7'] = { data: [], name: "設備B-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartBH7", showChart: false };
    $scope.baseData['BH8'] = { data: [], name: "設備B-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartBH8", showChart: false };
    $scope.baseData['BH9'] = { data: [], name: "設備B-房子線 9", chart: "顯示房子線 9 表格", chartId: "chartBH9", showChart: false };
    $scope.baseData['BW1'] = { data: [], name: "設備B-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartBW1", showChart: false };
    $scope.baseData['BW2'] = { data: [], name: "設備B-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartBW2", showChart: false };
    $scope.baseData['BW3'] = { data: [], name: "設備B-圍牆線 3", chart: "顯示房子線 3 表格", chartId: "chartBW3", showChart: false };
    $scope.baseData['BW4'] = { data: [], name: "設備B-圍牆線 4", chart: "顯示圍牆線 4 表格", chartId: "chartBW4", showChart: false };

    $scope.getBase = function (line,hour) {
        $scope.baseData.length=0;
        displacementService.getBase(line, hour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {

                for(i in data.data){
                    data.data[i].x =  Math.round((311471.92 +  data.data[i].x)*100)/100;
                    data.data[i].y =  Math.round((2771628.85 +  data.data[i].y)*100)/100;
                    data.data[i].z =  Math.round((15.97 +  data.data[i].z)*100)/100;
                }
                $scope.baseData[line].data = data.data;
               
            }
        });
    }

  
    $scope.setHours = function () {
        for (let i = 0; i < 24; i++) {
            $scope.hours.push(i);
        }
    }

    $scope.search = function () {
        for(let i in $scope.lineType){
            $scope.getBase($scope.lineType[i],$scope.selectedHour);
        }
    }

    $scope.init = function () {
        for(let i in $scope.lineType){
            $scope.getBase($scope.lineType[i],0);
        }
        $scope.setHours();
    }

    $scope.init();
});