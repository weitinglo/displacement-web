import * as THREE from '../../build/three.module.js';
import { TrackballControls } from '../../jsm/controls/TrackballControls.js';
import { OBJLoader2 } from '../../jsm/loaders/OBJLoader2.js';
import { MTLLoader } from '../../jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from '../../jsm/loaders/obj2/bridge/MtlObjBridge.js';

app.controller('liveCompareController', function ($scope, displacementService) {
    updateMenu('live_compare');
    var camera, controls, scene, renderer;
    var mouse = new THREE.Vector2();
    var raycaster = new THREE.Raycaster();
    const manager = new THREE.LoadingManager();
    $scope.loadingMsg = "資料讀取中請稍後";
    $scope.loading = true;
    $scope.selectedHour = 6;
    $scope.curData = [];
    $scope.preData = [];
    var curDataMap = new Map();
    var preDataMap = new Map();
    $scope.compareData = [];
    
    $scope.disArr = [1,2,3,4,5];
    $scope.selectedDisplacement = $scope.disArr[0];
    $scope.getCompareWhole = function (hour, date) {
        displacementService.getCompareWhole(hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                $scope.curData = data.data;
                displacementService.getCompareWhole(hour - 1, date).then(function (predata) {
                    if (predata.code && predata.code != 200) {
                        chiperUtils.showErrorNotification('top', 'center', predata.msg)
                    } else {
                        $scope.preData = predata.data;
                        setupData();
                    }
                });
            }
        });
    }

    function calDisplacement(x1, y1, x2, y2) {
        let difference_x = x1 == x2 ? 0 : Math.pow(x1 - x2, 2);
        let difference_y = y1 == y2 ? 0 : Math.pow(y1 - y2, 2);
        return (difference_x + difference_y) == 0 ? 0.0 : Math.sqrt(difference_x + difference_y);
    }
    function getDirection(preX, curX) {
        if (preX > curX)
            return -1;
        else if (preX < curX)
            return 1;
        return 0;
    }
    function setupData() {
        for (let i in $scope.curData) {
            let key = $scope.curData[i].name + "-" + $scope.curData[i].section;
            curDataMap.set(key, { x: $scope.curData[i].compare_x, y: $scope.curData[i].compare_y, base_x: $scope.curData[i].base_x, base_y: $scope.curData[i].base_y, base_z: $scope.curData[i].base_z });
        }
        for (let i in $scope.preData) {
            let key = $scope.preData[i].name + "-" + $scope.preData[i].section;
            preDataMap.set(key, { x: $scope.preData[i].compare_x, y: $scope.preData[i].compare_y });
        }

        for (let key of curDataMap.keys()) {
            if (preDataMap.has(key)) {
                let dis = calDisplacement(curDataMap.get(key).x, curDataMap.get(key).y, preDataMap.get(key).x, preDataMap.get(key).y);
                let direction = getDirection(preDataMap.get(key).x, curDataMap.get(key).x);
                let dataJson = {
                    displacement: dis * 100,
                    direction: direction,
                    base_x: curDataMap.get(key).base_x,
                    base_y: curDataMap.get(key).base_y,
                    base_z: curDataMap.get(key).base_z
                }
                $scope.compareData.push(dataJson);
            }
        }
       
        drawCoords($scope.compareData);
    }


    $scope.getSearchDateFormat = function (d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }

    $scope.search = function(){
        $scope.loading = true;
        $scope.threeJSinit();
        drawCoords($scope.compareData);

    }
    /*************Three JS******************/
    function get_gradient_color(coord_z, direction) {
        if (direction == 1) { //內縮
            if (coord_z > 1 & coord_z <= 3) {
                return [0, 255, 0];
            } else if (coord_z > 3 & coord_z <= 5) {
                return [255, 255, 0];
            } else if (coord_z > 5) {
                return [255, 0, 0];
            } else {
                return [0, 0, 0];
            }
        } else if (direction == -1) {  //外擴
            if (coord_z > 1 & coord_z <= 3) {
                return [0, 255, 255];
            } else if (coord_z > 3 & coord_z <= 5) {
                return [0, 0, 255];
            } else if (coord_z > 5) {
                return [255, 0, 255];
            } else {
                return [0, 0, 0];
            }
        } else {
            return [0, 0, 0];
        }

    }
    function drawCoords(coords_array) {
        for (var i = 0; i < coords_array.length; i++) {
            if(Math.abs(coords_array[i].displacement>$scope.selectedDisplacement) && coords_array[i].direction!=0){
                const coord_color = get_gradient_color(coords_array[i].displacement, coords_array[i].direction);
                let color = "rgb(" + coord_color[0] + "," + coord_color[1] + "," + coord_color[2] + ")";
                const material = new THREE.MeshPhongMaterial({
                    color: color
                });
                const geometry = new THREE.BoxGeometry(0.15, 0.15, 0.15);
                const cube = new THREE.Mesh(geometry, material);
                cube.position.set(parseFloat(coords_array[i].base_x), parseFloat(coords_array[i].base_y), parseFloat(coords_array[i].base_z));
                scene.add(cube);
            }
           
        }
        controls.update();
        render();
    }

    function animate() {
        requestAnimationFrame(animate);
        controls.update();
    }

    function render() {
        renderer.render(scene, camera);
    }
    manager.onStart = function () {
        $scope.loadingMsg = "3D 檔讀取中";
        $scope.$apply();
    };

    manager.onProgress = function () {
        $scope.loadingMsg = "3D 建模中";
        $scope.$apply();
    };
    manager.onLoad = function () {
        $scope.loading = false;
        $scope.$apply()
    };
    function draw_house_model(model_name) {
        var mtlLoader = new MTLLoader();
        mtlLoader.load('../chiper_model/' + model_name + '.mtl', (mtlParseResult) => {
            const materials = MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);
            var objLoader = new OBJLoader2(manager);
            objLoader.addMaterials(materials);
            objLoader.load('../chiper_model/' + model_name + '.obj', (root) => {
                scene.add(root);
                root.position.set(0, -0.1, -0.15);
                render();
            });
        });
    }
    function put_instruction() {
        var text2 = document.createElement('div');
        text2.style.position = 'absolute';
        text2.style.width = 100;
        text2.style.height = 100;
        text2.style.color = "white";
        text2.innerHTML = "【操作說明】<br>滑鼠左鍵：拖曳<br>滑鼠右鍵：平移<br>滑鼠滾輪：前後移動";
        text2.style.top = 30 + 'px';
        text2.style.left = 35 + 'px';
        document.getElementById('live_canvas').appendChild(text2);
    }

    function clearChart() {
        $scope.showChart.hour = false;
        $scope.showChart.day = false;

        const chartBlock = document.getElementById('barChartBlock');
        chartBlock.innerHTML = "";
        const chart24Block = document.getElementById('barChart24Block');
        chart24Block.innerHTML = "";
    }
    function doubleClick(event) {
        clearChart();
        const menu_WIDTH = 275;
        const HEIGHT = 535;
        mouse.x = ((event.clientX - 275) / (window.innerWidth - menu_WIDTH)) * 2 - 1;
        mouse.y = - ((event.clientY - 35) / HEIGHT) * 2 + 1;
        raycaster.setFromCamera(mouse, camera);
        let intersects = raycaster.intersectObjects(scene.children);
        if (intersects.length < 1) return;
        let selectedSection = "";
        let selectedName = "";
        for (let i in $scope.compareData) {
            let x = $scope.compareData[i].base_x;
            let y = $scope.compareData[i].base_y;
            let z = $scope.compareData[i].base_z;
            if (x == intersects[0].object.position.x && y == intersects[0].object.position.y && z == intersects[0].object.position.z) {
                selectedSection = $scope.compareData[i].section;
                selectedName = $scope.compareData[i].name;

            }

        }
        $scope.getCompareSection(selectedSection, selectedName, $scope.selectedHour);
        $scope.getCompareSectionHour(selectedSection, selectedName, $scope.selectedHour);
    }

    let INTERSECTED;
    function mouseOver(event) {
        try {
            if (window.scrollY != 0)
                scrollWin();
            const menu_WIDTH = 275;
            const HEIGHT = 535;
            mouse.x = ((event.clientX - 275) / (window.innerWidth - menu_WIDTH)) * 2 - 1;
            mouse.y = - ((event.clientY - 38) / HEIGHT) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);
            let intersects = raycaster.intersectObjects(scene.children);

            if (intersects.length < 1) return;
            if (intersects.length > 0) {
                if (INTERSECTED != intersects[0].object) {
                    if (INTERSECTED && INTERSECTED.material && INTERSECTED.material.emissive) {
                        INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
                    }
                    INTERSECTED = intersects[0].object;
                    INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
                    INTERSECTED.material.emissive.setHex(0xff0000);
                }
            } else {
                if (INTERSECTED) INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
                INTERSECTED = null;
            }
            renderer.render(scene, camera);
        } catch (err) {
            console.log(err)
        }

    }

    $scope.threeJSinit = function () {
        const menu_WIDTH = 275;
        const HEIGHT = 535;
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x000000);
        camera = new THREE.PerspectiveCamera(20, (window.innerWidth - menu_WIDTH) / HEIGHT, 1, 9000);
        camera.position.x = 73;
        camera.position.y = 15;
        camera.position.z = 35;
        camera.up.set(0, 0, 1);
        scene.add(camera);
        var light = new THREE.DirectionalLight(0xFFFFFF, 1.2, 1000); // white, intensity=1, distance = 500
        light.position.set(10, 0, 25); // (x, y, z)
        scene.add(light);
        const axesHelper = new THREE.AxesHelper(5);
        scene.add(axesHelper);

        var models = ['Tile_+002_+002_+000', 'Tile_+003_+001_+000', 'Tile_+003_+002_+000']
        models.forEach(element => draw_house_model(element));
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor("#e5e5e5");
        renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
        //renderer.domElement.addEventListener("dblclick", doubleClick);
        renderer.domElement.addEventListener("mousemove", mouseOver);

        const the_canvas = document.getElementById('live_canvas');
        the_canvas.innerHTML = "";
        the_canvas.appendChild(renderer.domElement);
        put_instruction();

        window.addEventListener('resize', () => {
            renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
            camera.aspect = (window.innerWidth - menu_WIDTH) / HEIGHT;

            camera.updateProjectionMatrix();
        })

        controls = new TrackballControls(camera, the_canvas);
        controls.rotateSpeed = 2;
        controls.zoomSpeed = 5.0;
        controls.noZoom = false;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        controls.target = new THREE.Vector3(40, -18, 15)
        render();
        controls.addEventListener('change', render);
        controls.update();
        animate();

    }

    function scrollWin() {
        window.scrollTo(0, 0);
    }

    $scope.init = function () {
        $scope.selectedDate = new Date('2020-11-02');
        $scope.threeJSinit();
        $scope.getCompareWhole($scope.selectedHour, $scope.getSearchDateFormat($scope.selectedDate));
    }
    $scope.init();
});

