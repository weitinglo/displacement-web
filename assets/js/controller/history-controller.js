import * as THREE from '../../build/three.module.js';
import { TrackballControls } from '../../jsm/controls/TrackballControls.js';
import { OBJLoader2 } from '../../jsm/loaders/OBJLoader2.js';
import { MTLLoader } from '../../jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from '../../jsm/loaders/obj2/bridge/MtlObjBridge.js';

app.controller('historyController', function ($scope, displacementService) {

    updateMenu('history');
    var camera, controls, scene, renderer;
    var mouse = new THREE.Vector2();
    var raycaster = new THREE.Raycaster();
    const manager = new THREE.LoadingManager();
    $scope.loadingMsg = "資料讀取中請稍後";
    $scope.loading = false;
    $scope.selectedHour = "";
    $scope.selectedDate = null;
    $scope.hours = [];
    $scope.compareData = [];
    $scope.compareSectionData = [];
    $scope.compareSection24Data = [];

    $scope.showChart = {
        day: false,
        hour: false
    }
    var chart = "";
    var chart24 = "";

    $scope.getCompareSectionHour = function (section, name, hour) {
        $scope.compareSection24Data.length=0;
        displacementService.getCompareSectionHour(section, name, hour, $scope.getSearchDateFormat($scope.selectedDate)).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {

                $scope.compareSection24Data = data.data;
            }
        });
    }
    $scope.getCompareSection = function (section, name, hour) {
        $scope.compareSectionData.length=0;
        displacementService.getCompareSection(section, name, hour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                $scope.compareSectionData = data.data;
            }
        });
    }

    $scope.getCompareWhole = function (hour, date) {
        displacementService.getCompareWhole(hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                drawCoords(data.data);
                $scope.compareData = data.data;
            }
        });
    }

    $scope.setHours = function () {
        for (let i = 0; i < 24; i++) {
            let jsonH = {
                name:i+ " 點",
                value:i
            }
            $scope.hours.push(jsonH);
        }
        $scope.selectedHour = $scope.hours[0];
    }
    $scope.getSearchDateFormat = function (d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }
    $scope.search = function () {
        $scope.compareData.length=0;
        $scope.compareSectionData.length=0;
        $scope.compareSection24Data .length=0;
        clearChart();
        if ($scope.selectedHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧時間");
            return;
        }
        if ($scope.selectedDate == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧日期");
            return;
        }
        $scope.loading = true;
        $scope.threeJSinit();
        $scope.getCompareWhole($scope.selectedHour.value, $scope.getSearchDateFormat($scope.selectedDate));

    }

    function clearChart(){
        $scope.showChart.hour= false;
        $scope.showChart.day= false;

        const chartBlock = document.getElementById('barChartBlock');
        chartBlock.innerHTML = "";
        const chart24Block = document.getElementById('barChart24Block');
        chart24Block.innerHTML = "";
    }

    $scope.plot = function () {
        if ($scope.showChart.day) {
            const chartBlock = document.getElementById('barChartBlock');
            var canvas = document.createElement('canvas');
            canvas.id = "barChart";
            canvas.width = 400;
            canvas.height = 80;
            chartBlock.appendChild(canvas);

            let ctx = document.getElementById("barChart").getContext('2d');
            let chartDataArr = [];
            for (let i = 0; i < $scope.compareSectionData.length; i++) {
                chartDataArr.push($scope.compareSectionData[i].displacement * $scope.compareSectionData[i].direction);
            }
            let labelArr = [];
            for (let i = 0; i < $scope.compareSectionData.length; i++) {
                labelArr.push($scope.compareSectionData[i].date.substring(0, 4) + "/" + $scope.compareSectionData[i].date.substring(4, 6) + "/" + $scope.compareSectionData[i].date.substring(6, 8));
            }
            let options = {
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "日期",
                        }
                    }],
                    yAxes: [{

                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "變位量（公分）",
                        }
                    }]
                }

            }
            let chartDataSetting = {
                labels: labelArr, datasets: [{ label: "同時段每日變位量", data: chartDataArr, backgroundColor: 'green', borderWidth: 1 }]
            }
            chart = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chart.update();
        }
    }

    $scope.plot24 = function () {
        if ($scope.showChart.hour) {
            const chartBlock = document.getElementById('barChart24Block');
            var canvas = document.createElement('canvas');
            canvas.id = "barChart24";
            canvas.width = 400;
            canvas.height = 80;
            chartBlock.appendChild(canvas);

            let ctx = document.getElementById("barChart24").getContext('2d');
            let chartDataArr = [];
            for (let i = 0; i < $scope.compareSection24Data.length; i++) {
                chartDataArr.push($scope.compareSection24Data[i].displacement * $scope.compareSection24Data[i].direction);
            }
            let labelArr = [];
            for (let i = 0; i < $scope.compareSection24Data.length; i++) {
                let dateData = $scope.compareSection24Data[i].date;

                labelArr.push(dateData.substring(0, 4) + "/" + dateData.substring(4, 6) + "/" + dateData.substring(6, 8) + " " + $scope.compareSection24Data[i].time + "點");
            }
            let options = {
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "時段",
                        }
                    }],
                    yAxes: [{

                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "變位量（公分）",
                        }
                    }]
                }

            }
            let chartDataSetting = {
                labels: labelArr, datasets: [{ label: "24小時內變位量", data: chartDataArr, backgroundColor: 'orange', borderWidth: 1 }]
            }
            chart24 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chart24.update();
        }
    }

    /*************Three JS******************/
    function get_gradient_color(coord_z, direction) {
        if (direction == 1) { //內縮
            if (coord_z > 1 & coord_z <= 3) {
                return [0, 255, 0];
            } else if (coord_z > 3 & coord_z <= 5) {
                return [255, 255, 0];
            } else if (coord_z > 5) {
                return [255, 0, 0];
            } else {
                return [0, 0, 0];
            }
        } else if (direction == -1) {  //外擴
            if (coord_z > 1 & coord_z <= 3) {
                return [0, 255, 255];
            } else if (coord_z > 3 & coord_z <= 5) {
                return [0, 0, 255];
            } else if (coord_z > 5) {
                return [255, 0, 255];
            } else {
                return [0, 0, 0];
            }
        } else {
            return [0, 0, 0];
        }
    }
    function drawCoords(coords_array) {
        for (var i = 0; i < coords_array.length; i++) {
            if (coords_array[i].displacement > 1) {
                const coord_color = get_gradient_color(coords_array[i].displacement,coords_array[i].direction);
                let color = "rgb(" + coord_color[0] + "," + coord_color[1] + "," + coord_color[2] + ")";
                const material = new THREE.MeshPhongMaterial({
                    color: color
                });
                const geometry = new THREE.BoxGeometry(0.15, 0.15, 0.15);
                const cube = new THREE.Mesh(geometry, material);
                cube.position.set(parseFloat(coords_array[i].base_x), parseFloat(coords_array[i].base_y), parseFloat(coords_array[i].base_z));
                scene.add(cube);
            }
        }
        controls.update();
        render();
    }

    function animate() {
        requestAnimationFrame(animate);
        controls.update();
    }

    function render() {
        renderer.render(scene, camera);
    }
    manager.onStart = function () {
        $scope.loadingMsg = "3D 檔讀取中";
        $scope.$apply();
    };

    manager.onProgress = function () {
        $scope.loadingMsg = "3D 建模中";
        $scope.$apply();
    };
    manager.onLoad = function () {
        $scope.loading = false;
        $scope.$apply()
    };

    function draw_house_model(model_name) {
        var mtlLoader = new MTLLoader();
        mtlLoader.load('../chiper_model/' + model_name + '.mtl', (mtlParseResult) => {
            const materials = MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);
            var objLoader = new OBJLoader2(manager);
            objLoader.addMaterials(materials);

            objLoader.load('../chiper_model/' + model_name + '.obj', (root) => {
                scene.add(root);
                root.position.set(0, -0.1, -0.15);
                render();
            });
        });
    }
    function put_instruction() {
        var text2 = document.createElement('div');
        text2.style.position = 'absolute';
        text2.style.width = 100;
        text2.style.height = 100;
        text2.style.color = "white";
        text2.innerHTML = "【操作說明】<br>滑鼠左鍵：拖曳<br>滑鼠右鍵：平移<br>滑鼠滾輪：前後移動<br>滑鼠左鍵雙擊：選擇線段";
        text2.style.top = 30 + 'px';
        text2.style.left = 35 + 'px';
        document.getElementById('history_canvas').appendChild(text2);
    }

    var preX=null;
    var preY=null;
    var preColor = null;
    var selectedPoint = null;
    function doubleClick(event) {
        clearChart();
        if(selectedPoint)
            selectedPoint.material.emissive.setHex( preColor );
        const HEIGHT = 535;
        mouse.x = ((event.clientX-275) / (window.innerWidth - 300)) * 2 - 1;
        mouse.y = - ((event.clientY-35) / HEIGHT) * 2 + 1;
        preX =  ((event.clientX-275) / (window.innerWidth - 300)) * 2 - 1;
        preY = - ((event.clientY-35) / HEIGHT) * 2 + 1;
        raycaster.setFromCamera(mouse, camera);
        let intersects = raycaster.intersectObjects(scene.children);
        if (intersects.length < 1) return;
        if ( intersects.length > 0 ) {
            if ( selectedPoint != intersects[0].object ) {
               
                selectedPoint = intersects[ 0 ].object;
                selectedPoint.currentHex = selectedPoint.material.emissive.getHex();
                preColor = intersects[ 0 ].object.material.emissive;
                selectedPoint.material.emissive.setHex( 0xff0000 );
                
            }
        } 

        let selectedSection = "";
        let selectedName = "";
        for (let i in $scope.compareData) {
            let x = $scope.compareData[i].base_x;
            let y = $scope.compareData[i].base_y;
            let z = $scope.compareData[i].base_z;
            if (x == intersects[0].object.position.x && y == intersects[0].object.position.y && z == intersects[0].object.position.z) {
                selectedSection = $scope.compareData[i].section;
                selectedName = $scope.compareData[i].name;

            }

        }
        $scope.getCompareSection(selectedSection, selectedName, $scope.selectedHour.value);
        $scope.getCompareSectionHour(selectedSection, selectedName, $scope.selectedHour.value);
        chiperUtils.showNotification('top', 'center', "已選擇點雲線段");

    }

    let INTERSECTED;
    function mouseOver(event) {
        try {
            if (window.scrollY != 0)
                scrollWin();
            const HEIGHT = 535;
            mouse.x = ((event.clientX - 275) / (window.innerWidth - 300)) * 2 - 1;
            mouse.y = - ((event.clientY - 38) / HEIGHT) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);
            let intersects = raycaster.intersectObjects(scene.children);

            if (intersects.length < 1) return;
            if (intersects.length > 0) {
                if (INTERSECTED != intersects[0].object) {
                    if (INTERSECTED && INTERSECTED.material && INTERSECTED.material.emissive) {
                        INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
                    }
                    INTERSECTED = intersects[0].object;
                    INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
                    INTERSECTED.material.emissive.setHex(0xff0000);
                }
            } else {
                if (INTERSECTED) INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
                INTERSECTED = null;
            }
            renderer.render(scene, camera);
        } catch (err) {
            console.log(err)
        }
    }

    $scope.threeJSinit = function () {
        const menu_WIDTH = 300;
        const HEIGHT = 535;
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x000000);
        camera = new THREE.PerspectiveCamera(20, (window.innerWidth - menu_WIDTH) / HEIGHT, 1, 9000);
        camera.position.x = 73;
        camera.position.y = 15;
        camera.position.z = 35;
        camera.up.set(0, 0, 1);
        scene.add(camera);
        var light = new THREE.DirectionalLight(0xFFFFFF, 1.5, 1000); // white, intensity=1, distance = 500
        light.position.set(10, 0, 25); // (x, y, z)
        scene.add(light);
        const axesHelper = new THREE.AxesHelper(5);
        scene.add(axesHelper);

        var models = ['Tile_+002_+002_+000', 'Tile_+003_+001_+000', 'Tile_+003_+002_+000']
        models.forEach(element => draw_house_model(element));
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor("#e5e5e5");
        renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
        renderer.domElement.addEventListener("dblclick", doubleClick);
        renderer.domElement.addEventListener("mousemove", mouseOver, false);

        const the_canvas = document.getElementById('history_canvas');
        the_canvas.innerHTML = "";
        the_canvas.appendChild(renderer.domElement);
        put_instruction();

        window.addEventListener('resize', () => {
            renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
            camera.aspect = (window.innerWidth - menu_WIDTH) / HEIGHT;

            camera.updateProjectionMatrix();
        })

        controls = new TrackballControls(camera, the_canvas);
        controls.rotateSpeed = 2;
        controls.zoomSpeed = 5.0;
        controls.noZoom = false;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        controls.target = new THREE.Vector3(40, -18, 15)

        render();
        controls.addEventListener('change', render);
        controls.update();
        animate();
    }


    function scrollWin() {
        window.scrollTo(0, 0);
    }

    /*******************Done Three JS**************/
    $scope.init = function () {
        $scope.setHours();
    }
    $scope.init();
});

