
app.controller('homeController', function ($scope, displacementService) {
    updateMenu('home');
    $scope.selectedHour = 0;
    $scope.selectedDate = ['20201012', '20201011', '20201010', '20201009', '20201008'];
    $scope.hours = [];
    $scope.preLine = 0;

    $scope.loading=true;

    $scope.showAH1 = false;
    $scope.baseAH1Data_0 = [];
    $scope.baseAH1Data_1 = [];
    $scope.baseAH1Data_2 = [];
    $scope.baseAH1Data_3 = [];
    $scope.baseAH1Data_4 = [];
    $scope.baseAH1Data_5 = [];

    $scope.showAH2 = false;
    $scope.baseAH2Data_0 = [];
    $scope.baseAH2Data_1 = [];
    $scope.baseAH2Data_2 = [];
    $scope.baseAH2Data_3 = [];
    $scope.baseAH2Data_4 = [];
    $scope.baseAH2Data_5 = [];

    $scope.showAH3 = false;
    $scope.baseAH3Data_0 = [];
    $scope.baseAH3Data_1 = [];
    $scope.baseAH3Data_2 = [];
    $scope.baseAH3Data_3 = [];
    $scope.baseAH3Data_4 = [];

    $scope.showAH4 = false;
    $scope.baseAH4Data_0 = [];
    $scope.baseAH4Data_1 = [];
    $scope.baseAH4Data_2 = [];
    $scope.baseAH4Data_3 = [];
    $scope.baseAH4Data_4 = [];
    $scope.baseAH4Data_5 = [];

    $scope.showAH5 = false;
    $scope.baseAH5Data_0 = [];
    $scope.baseAH5Data_1 = [];
    $scope.baseAH5Data_2 = [];
    $scope.baseAH5Data_3 = [];
    $scope.baseAH5Data_4 = [];
    $scope.baseAH5Data_5 = [];

    $scope.showAW1 = false;
    $scope.baseAW1Data_0 = [];
    $scope.baseAW1Data_1 = [];
    $scope.baseAW1Data_2 = [];
    $scope.baseAW1Data_3 = [];
    $scope.baseAW1Data_4 = [];
    $scope.baseAW1Data_5 = [];

    $scope.showAW2 = false;
    $scope.baseAW2Data_0 = [];
    $scope.baseAW2Data_1 = [];
    $scope.baseAW2Data_2 = [];
    $scope.baseAW2Data_3 = [];
    $scope.baseAW2Data_4 = [];
    $scope.baseAW2Data_5 = [];

    $scope.showAW3 = false;
    $scope.baseAW3Data_0 = [];
    $scope.baseAW3Data_1 = [];
    $scope.baseAW3Data_2 = [];
    $scope.baseAW3Data_3 = [];
    $scope.baseAW3Data_4 = [];
    $scope.baseAW3Data_5 = [];

    $scope.getHomeBehaviorAW1 = function (hour, date, index) {
        displacementService.getHomeBehavior('AW1', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAW1Data_0 = data.data
                else if (index == 1)
                    $scope.baseAW1Data_1 = data.data
                else if (index == 2)
                    $scope.baseAW1Data_2 = data.data
                else if (index == 3)
                    $scope.baseAW1Data_3 = data.data
                else if (index == 4)
                    $scope.baseAW1Data_4 = data.data

            }
        });
    }
    $scope.getHomeBehaviorAW2 = function (hour, date, index) {
        displacementService.getHomeBehavior('AW2', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAW2Data_0 = data.data
                else if (index == 1)
                    $scope.baseAW2Data_1 = data.data
                else if (index == 2)
                    $scope.baseAW2Data_2 = data.data
                else if (index == 3)
                    $scope.baseAW2Data_3 = data.data
                else if (index == 4)
                    $scope.baseAW2Data_4 = data.data
            }
        });
    }
    $scope.getHomeBehaviorAW3 = function (hour, date, index) {
        displacementService.getHomeBehavior('AW3', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAW3Data_0 = data.data
                else if (index == 1)
                    $scope.baseAW3Data_1 = data.data
                else if (index == 2)
                    $scope.baseAW3Data_2 = data.data
                else if (index == 3)
                    $scope.baseAW3Data_3 = data.data
                else if (index == 4)
                    $scope.baseAW3Data_4 = data.data
            }

        });
    }

    $scope.getHomeBehaviorAH1 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH1', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAH1Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH1Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH1Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH1Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH1Data_4 = data.data
            }

        });
    }
    $scope.getHomeBehaviorAH2 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH2', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                $scope.baseAH2Data = data.data;
                if (index == 0)
                    $scope.baseAH2Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH2Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH2Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH2Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH2Data_4 = data.data
            }
        });
    }
    $scope.getHomeBehaviorAH3 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH3', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAH3Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH3Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH3Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH3Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH3Data_4 = data.data
            }
        });
    }
    $scope.getHomeBehaviorAH4 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH4', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAH4Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH4Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH4Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH4Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH4Data_4 = data.data
            }
        });
    }
    $scope.getHomeBehaviorAH5 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH5', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAH5Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH5Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH5Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH5Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH5Data_4 = data.data
            }
        });
    }
    $scope.getHomeBehaviorAH6 = function (hour, date, index) {
        displacementService.getHomeBehavior('AH6', hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                for(i in data.data){
                    data.data[i].displacement = data.data[i].displacement*data.data[i].direction;     
                }
                if (index == 0)
                    $scope.baseAH6Data_0 = data.data
                else if (index == 1)
                    $scope.baseAH6Data_1 = data.data
                else if (index == 2)
                    $scope.baseAH6Data_2 = data.data
                else if (index == 3)
                    $scope.baseAH6Data_3 = data.data
                else if (index == 4)
                    $scope.baseAH6Data_4 = data.data
            }
        });
    }
    $scope.findConstantChange = function(){
        $scope.disAH1 = [];
        for(i in $scope.baseAH1Data_0){
            if(Math.abs($scope.baseAH1Data_0[i].displacement)>0.5){
                $scope.disAH1.push($scope.baseAH1Data_0[i].section);
            }
        }
      
        for(i in $scope.disAH1){
            let section = $scope.disAH1[i];
            for(j in $scope.baseAH1Data_1){
                if(($scope.baseAH1Data_1[j].section == section)&& ((Math.abs($scope.baseAH1Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH1Data_1[j].displacement)>10))){
                    $scope.disAH1[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH1Data_2){
                if(($scope.baseAH1Data_2[j].section == section)&& ((Math.abs($scope.baseAH1Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH1Data_2[j].displacement)>10))){
                    $scope.disAH1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH1Data_3){
                if(($scope.baseAH1Data_3[j].section == section)&& ((Math.abs($scope.baseAH1Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH1Data_3[j].displacement)>10))){
                    $scope.disAH1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH1Data_4){
                if(($scope.baseAH1Data_4[j].section == section)&& ((Math.abs($scope.baseAH1Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH1Data_4[j].displacement)>10))){
                    $scope.disAH1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH1Data_5){
                if(($scope.baseAH1Data_5[j].section == section)&& ((Math.abs($scope.baseAH1Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAH1Data_5[j].displacement)>10))){
                    $scope.disAH1[i]=-100;
                    continue;
                }
            }
          
            
        }
        let sectionLen = 0;
        for(i in $scope.disAH1){
            if($scope.disAH1[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAH1[i])
        }
        sectionLen=sectionLen+5;
        let data_AH1_1=new Array(sectionLen);
        let data_AH1_2=new Array(sectionLen);
        let data_AH1_3=new Array(sectionLen);
        let data_AH1_4=new Array(sectionLen);
        let data_AH1_5=new Array(sectionLen);

        for(i in $scope.disAH1){
            let section = $scope.disAH1[i];
            if(section!=-100){
                for(j in $scope.baseAH1Data_0){
                    if($scope.baseAH1Data_0[j].section==section)
                        data_AH1_1[section]=$scope.baseAH1Data_0[j].displacement;
                }
                for(j in $scope.baseAH1Data_1){
                    if($scope.baseAH1Data_1[j].section==section)
                        data_AH1_2[section]=$scope.baseAH1Data_1[j].displacement;
                }
                for(j in $scope.baseAH1Data_2){
                    if($scope.baseAH1Data_2[j].section==section)
                        data_AH1_3[section]=$scope.baseAH1Data_2[j].displacement;
                }
                for(j in $scope.baseAH1Data_3){
                    if($scope.baseAH1Data_3[j].section==section)
                        data_AH1_4[section]=$scope.baseAH1Data_3[j].displacement;
                }
                for(j in $scope.baseAH1Data_4){
                    if($scope.baseAH1Data_4[j].section==section)
                        data_AH1_5[section]=$scope.baseAH1Data_4[j].displacement;
                }
                $scope.showAH1 = true;
                $("#AH1_"+Math.floor(section/10)).css("fill","red");
                $("#AH1_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 1', 'AH1' , data_AH1_1,data_AH1_2,data_AH1_3,data_AH1_4,data_AH1_5,sectionLen);
        

        ////////////////AH2//////////////////
        $scope.disAH2 = [];
        for(i in $scope.baseAH2Data_0){
            if(Math.abs($scope.baseAH2Data_0[i].displacement)>0.5){
                $scope.disAH2.push($scope.baseAH2Data_0[i].section);
            }
        }
      
        for(i in $scope.disAH2){
            let section = $scope.disAH2[i];
            for(j in $scope.baseAH2Data_1){
                if(($scope.baseAH2Data_1[j].section == section)&& ((Math.abs($scope.baseAH2Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH2Data_1[j].displacement)>10))){
                    $scope.disAH2[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH2Data_2){
                if(($scope.baseAH2Data_2[j].section == section)&& ((Math.abs($scope.baseAH2Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH2Data_2[j].displacement)>10))){
                    $scope.disAH2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH2Data_3){
                if(($scope.baseAH2Data_3[j].section == section)&& ((Math.abs($scope.baseAH2Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH2Data_3[j].displacement)>10))){
                    $scope.disAH2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH2Data_4){
                if(($scope.baseAH2Data_4[j].section == section)&& ((Math.abs($scope.baseAH2Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH2Data_4[j].displacement)>10))){
                    $scope.disAH2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH2Data_5){
                if(($scope.baseAH2Data_5[j].section == section)&& ((Math.abs($scope.baseAH2Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAH2Data_5[j].displacement)>10))){
                    $scope.disAH2[i]=-100;
                    continue;
                }
            }
          
            
        }
        sectionLen = 0;
        for(i in $scope.disAH2){
            if($scope.disAH2[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAH2[i])
        }
        sectionLen=sectionLen+5;
        let data_AH2_1=new Array(sectionLen);
        let data_AH2_2=new Array(sectionLen);
        let data_AH2_3=new Array(sectionLen);
        let data_AH2_4=new Array(sectionLen);
        let data_AH2_5=new Array(sectionLen);

        for(i in $scope.disAH2){
            let section = $scope.disAH2[i];
            if(section!=-1){
                for(j in $scope.baseAH2Data_0){
                    if($scope.baseAH2Data_0[j].section==section)
                        data_AH2_1[section]=$scope.baseAH2Data_0[j].displacement;
                }
                for(j in $scope.baseAH2Data_1){
                    if($scope.baseAH2Data_1[j].section==section)
                        data_AH2_2[section]=$scope.baseAH2Data_1[j].displacement;
                }
                for(j in $scope.baseAH2Data_2){
                    if($scope.baseAH2Data_2[j].section==section)
                        data_AH2_3[section]=$scope.baseAH2Data_2[j].displacement;
                }
                for(j in $scope.baseAH2Data_3){
                    if($scope.baseAH2Data_3[j].section==section)
                        data_AH2_4[section]=$scope.baseAH2Data_3[j].displacement;
                }
                for(j in $scope.baseAH2Data_4){
                    if($scope.baseAH2Data_4[j].section==section)
                        data_AH2_5[section]=$scope.baseAH2Data_4[j].displacement;
                }
                $scope.showAH2 = true;
                $("#AH2_"+Math.floor(section/10)).css("fill","red");
                $("#AH2_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 2', 'AH2' , data_AH2_1,data_AH2_2,data_AH2_3,data_AH2_4,data_AH2_5,sectionLen);
        ////////////////AH3//////////////////
        $scope.disAH3 = [];
        for(i in $scope.baseAH3Data_0){
            if(Math.abs($scope.baseAH3Data_0[i].displacement)>0.5){
                $scope.disAH3.push($scope.baseAH3Data_0[i].section);
            }
        }
      
        for(i in $scope.disAH3){
            let section = $scope.disAH3[i];
            for(j in $scope.baseAH3Data_1){
                if(($scope.baseAH3Data_1[j].section == section)&& ((Math.abs($scope.baseAH3Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH3Data_1[j].displacement)>10))){
                    $scope.disAH3[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH3Data_2){
                if(($scope.baseAH3Data_2[j].section == section)&& ((Math.abs($scope.baseAH3Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH3Data_2[j].displacement)>10))){
                    $scope.disAH3[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH3Data_3){
                if(($scope.baseAH3Data_3[j].section == section)&& ((Math.abs($scope.baseAH3Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH3Data_3[j].displacement)>10))){
                    $scope.disAH3[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH3Data_4){
                if(($scope.baseAH3Data_4[j].section == section)&& ((Math.abs($scope.baseAH3Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH3Data_4[j].displacement)>10))){
                    $scope.disAH3[i]=-100;
                    continue;
                }
            }

          
            
        }
        sectionLen = 0;
        for(i in $scope.disAH3){
            if($scope.disAH3[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAH3[i])
        }
        sectionLen=sectionLen+5;
        let data_AH3_1=new Array(sectionLen);
        let data_AH3_2=new Array(sectionLen);
        let data_AH3_3=new Array(sectionLen);
        let data_AH3_4=new Array(sectionLen);
        let data_AH3_5=new Array(sectionLen);

        for(i in $scope.disAH3){
            let section = $scope.disAH3[i];
            if(section!=-1){
                for(j in $scope.baseAH3Data_0){
                    if($scope.baseAH3Data_0[j].section==section)
                        data_AH3_1[section]=$scope.baseAH3Data_0[j].displacement;
                }
                for(j in $scope.baseAH3Data_1){
                    if($scope.baseAH3Data_1[j].section==section)
                        data_AH3_2[section]=$scope.baseAH3Data_1[j].displacement;
                }
                for(j in $scope.baseAH3Data_2){
                    if($scope.baseAH3Data_2[j].section==section)
                        data_AH3_3[section]=$scope.baseAH3Data_2[j].displacement;
                }
                for(j in $scope.baseAH3Data_3){
                    if($scope.baseAH3Data_3[j].section==section)
                        data_AH3_4[section]=$scope.baseAH3Data_3[j].displacement;
                }
                for(j in $scope.baseAH3Data_4){
                    if($scope.baseAH3Data_4[j].section==section)
                        data_AH3_5[section]=$scope.baseAH3Data_4[j].displacement;
                }
                $scope.showAH3 = true;
                $("#AH3_"+Math.floor(section/10)).css("fill","red");
                $("#AH3_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 3', 'AH3' , data_AH3_1,data_AH3_2,data_AH3_3,data_AH3_4,data_AH3_5,sectionLen);
        ////////////////AH4//////////////////
        $scope.disAH4 = [];
        for(i in $scope.baseAH4Data_0){
            if(Math.abs($scope.baseAH4Data_0[i].displacement)>0.5){
                $scope.disAH4.push($scope.baseAH4Data_0[i].section);
            }
        }
      
        for(i in $scope.disAH4){
            let section = $scope.disAH4[i];
            for(j in $scope.baseAH4Data_1){
                if(($scope.baseAH4Data_1[j].section == section)&& ((Math.abs($scope.baseAH4Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH4Data_1[j].displacement)>10))){
                    $scope.disAH4[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH4Data_2){
                if(($scope.baseAH4Data_2[j].section == section)&& ((Math.abs($scope.baseAH4Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH4Data_2[j].displacement)>10))){
                    $scope.disAH4[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH4Data_3){
                if(($scope.baseAH4Data_3[j].section == section)&& ((Math.abs($scope.baseAH4Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH4Data_3[j].displacement)>10))){
                    $scope.disAH4[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH4Data_4){
                if(($scope.baseAH4Data_4[j].section == section)&& ((Math.abs($scope.baseAH4Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH4Data_4[j].displacement)>10))){
                    $scope.disAH4[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH4Data_5){
                if(($scope.baseAH4Data_5[j].section == section)&& ((Math.abs($scope.baseAH4Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAH4Data_5[j].displacement)>10))){
                    $scope.disAH4[i]=-100;
                    continue;
                }
            }
          
            
        }
        sectionLen = 0;
        for(i in $scope.disAH4){
            if($scope.disAH4[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAH4[i])
        }
        sectionLen=sectionLen+5;
        let data_AH4_1=new Array(sectionLen);
        let data_AH4_2=new Array(sectionLen);
        let data_AH4_3=new Array(sectionLen);
        let data_AH4_4=new Array(sectionLen);
        let data_AH4_5=new Array(sectionLen);

        for(i in $scope.disAH4){
            let section = $scope.disAH4[i];
            if(section!=-1){
                for(j in $scope.baseAH4Data_0){
                    if($scope.baseAH4Data_0[j].section==section)
                        data_AH4_1[section]=$scope.baseAH4Data_0[j].displacement;
                }
                for(j in $scope.baseAH4Data_1){
                    if($scope.baseAH4Data_1[j].section==section)
                        data_AH4_2[section]=$scope.baseAH4Data_1[j].displacement;
                }
                for(j in $scope.baseAH4Data_2){
                    if($scope.baseAH4Data_2[j].section==section)
                        data_AH4_3[section]=$scope.baseAH4Data_2[j].displacement;
                }
                for(j in $scope.baseAH4Data_3){
                    if($scope.baseAH4Data_3[j].section==section)
                        data_AH4_4[section]=$scope.baseAH4Data_3[j].displacement;
                }
                for(j in $scope.baseAH4Data_4){
                    if($scope.baseAH4Data_4[j].section==section)
                        data_AH4_5[section]=$scope.baseAH4Data_4[j].displacement;
                }
                $scope.showAH4 = true;
                $("#AH4_"+Math.floor(section/10)).css("fill","red");
                $("#AH4_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 4', 'AH4' , data_AH4_1,data_AH4_2,data_AH4_3,data_AH4_4,data_AH4_5,sectionLen);
        ////////////////AH5//////////////////
        $scope.disAH5 = [];
        for(i in $scope.baseAH5Data_0){
            if(Math.abs($scope.baseAH5Data_0[i].displacement)>0.5){
                $scope.disAH5.push($scope.baseAH5Data_0[i].section);
            }
        }
      
        for(i in $scope.disAH5){
            let section = $scope.disAH5[i];
            for(j in $scope.baseAH5Data_1){
                if(($scope.baseAH5Data_1[j].section == section)&& ((Math.abs($scope.baseAH5Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH5Data_1[j].displacement)>10))){
                    $scope.disAH5[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH5Data_2){
                if(($scope.baseAH5Data_2[j].section == section)&& ((Math.abs($scope.baseAH5Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH5Data_2[j].displacement)>10))){
                    $scope.disAH5[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH5Data_3){
                if(($scope.baseAH5Data_3[j].section == section)&& ((Math.abs($scope.baseAH5Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH5Data_3[j].displacement)>10))){
                    $scope.disAH5[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH5Data_4){
                if(($scope.baseAH5Data_4[j].section == section)&& ((Math.abs($scope.baseAH5Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH5Data_4[j].displacement)>10))){
                    $scope.disAH5[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH5Data_5){
                if(($scope.baseAH5Data_5[j].section == section)&& ((Math.abs($scope.baseAH5Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAH5Data_5[j].displacement)>10))){
                    $scope.disAH5[i]=-100;
                    continue;
                }
            }
        }
        sectionLen = 0;
        for(i in $scope.disAH5){
            if($scope.disAH5[i]!=-100){
                sectionLen = Math.max(sectionLen,$scope.disAH5[i])
                // let section = $scope.disAH5[i];
                // for(j in $scope.baseAH5Data_3){
                //     if($scope.baseAH5Data_3[j].section==section && ($scope.baseAH5Data_3[j].displacement>3)){
                //         console.log(section);
                //         displacementService.saveFilter('AH5',section,0.1,0);
                //     }
                // }
                // for(j in $scope.baseAH5Data_2){
                //     if($scope.baseAH5Data_2[j].section==section && ($scope.baseAH5Data_2[j].displacement>3)){
                //         console.log(section);
                //         displacementService.saveFilter('AH5',section,0.1,0);
                //     }
                // }
               
            }
                
        }
        sectionLen=sectionLen+5;
        let data_AH5_1=new Array(sectionLen);
        let data_AH5_2=new Array(sectionLen);
        let data_AH5_3=new Array(sectionLen);
        let data_AH5_4=new Array(sectionLen);
        let data_AH5_5=new Array(sectionLen);

        for(i in $scope.disAH5){
            let section = $scope.disAH5[i];
            if(section!=-1){
                for(j in $scope.baseAH5Data_0){
                    if($scope.baseAH5Data_0[j].section==section)
                        data_AH5_1[section]=$scope.baseAH5Data_0[j].displacement;
                }
                for(j in $scope.baseAH5Data_1){
                    if($scope.baseAH5Data_1[j].section==section)
                        data_AH5_2[section]=$scope.baseAH5Data_1[j].displacement;
                }
                for(j in $scope.baseAH5Data_2){
                    if($scope.baseAH5Data_2[j].section==section)
                        data_AH5_3[section]=$scope.baseAH5Data_2[j].displacement;
                }
                for(j in $scope.baseAH5Data_3){
                    if($scope.baseAH5Data_3[j].section==section)
                        data_AH5_4[section]=$scope.baseAH5Data_3[j].displacement;
                }
                for(j in $scope.baseAH5Data_4){
                    if($scope.baseAH5Data_4[j].section==section)
                        data_AH5_5[section]=$scope.baseAH5Data_4[j].displacement;
                }
                $scope.showAH5 = true;
                $("#AH5_"+Math.floor(section/10)).css("fill","red");
                $("#AH5_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 5', 'AH5' , data_AH5_1,data_AH5_2,data_AH5_3,data_AH5_4,data_AH5_5,sectionLen);
        ////////////////AH6//////////////////
        $scope.disAH6 = [];
        for(i in $scope.baseAH6Data_0){
            if(Math.abs($scope.baseAH6Data_0[i].displacement)>0.5){
                $scope.disAH6.push($scope.baseAH6Data_0[i].section);
            }
        }
        
        for(i in $scope.disAH6){
            let section = $scope.disAH6[i];
            for(j in $scope.baseAH6Data_1){
                if(($scope.baseAH6Data_1[j].section == section)&& ((Math.abs($scope.baseAH6Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAH6Data_1[j].displacement)>10))){
                    $scope.disAH6[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAH6Data_2){
                if(($scope.baseAH6Data_2[j].section == section)&& ((Math.abs($scope.baseAH6Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAH6Data_2[j].displacement)>10))){
                    $scope.disAH6[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH6Data_3){
                if(($scope.baseAH6Data_3[j].section == section)&& ((Math.abs($scope.baseAH6Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAH6Data_3[j].displacement)>10))){
                    $scope.disAH6[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH6Data_4){
                if(($scope.baseAH6Data_4[j].section == section)&& ((Math.abs($scope.baseAH6Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAH6Data_4[j].displacement)>10))){
                    $scope.disAH6[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAH6Data_5){
                if(($scope.baseAH6Data_5[j].section == section)&& ((Math.abs($scope.baseAH6Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAH6Data_5[j].displacement)>10))){
                    $scope.disAH6[i]=-100;
                    continue;
                }
            }
            
            
        }
        sectionLen = 0;
        for(i in $scope.disAH6){
            if($scope.disAH6[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAH6[i])
        }
        sectionLen=sectionLen+5;
        let data_AH6_1=new Array(sectionLen);
        let data_AH6_2=new Array(sectionLen);
        let data_AH6_3=new Array(sectionLen);
        let data_AH6_4=new Array(sectionLen);
        let data_AH6_5=new Array(sectionLen);

        for(i in $scope.disAH6){
            let section = $scope.disAH6[i];
            if(section!=-1){
                for(j in $scope.baseAH6Data_0){
                    if($scope.baseAH6Data_0[j].section==section)
                        data_AH6_1[section]=$scope.baseAH6Data_0[j].displacement;
                }
                for(j in $scope.baseAH6Data_1){
                    if($scope.baseAH6Data_1[j].section==section)
                        data_AH6_2[section]=$scope.baseAH6Data_1[j].displacement;
                }
                for(j in $scope.baseAH6Data_2){
                    if($scope.baseAH6Data_2[j].section==section)
                        data_AH6_3[section]=$scope.baseAH6Data_2[j].displacement;
                }
                for(j in $scope.baseAH6Data_3){
                    if($scope.baseAH6Data_3[j].section==section)
                        data_AH6_4[section]=$scope.baseAH6Data_3[j].displacement;
                }
                for(j in $scope.baseAH6Data_4){
                    if($scope.baseAH6Data_4[j].section==section)
                        data_AH6_5[section]=$scope.baseAH6Data_4[j].displacement;
                }
                $scope.showAH6 = true;
                $("#AH6_"+Math.floor(section/10)).css("fill","red");
                $("#AH6_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-房子線 6', 'AH6' , data_AH6_1,data_AH6_2,data_AH6_3,data_AH6_4,data_AH6_5,sectionLen);
        ////////////////AW1//////////////////
        $scope.disAW1 = [];
        for(i in $scope.baseAW1Data_0){
            if(Math.abs($scope.baseAW1Data_0[i].displacement)>0.5){
                $scope.disAW1.push($scope.baseAW1Data_0[i].section);
            }
        }
      
        for(i in $scope.disAW1){
            let section = $scope.disAW1[i];
            for(j in $scope.baseAW1Data_1){
                if(($scope.baseAW1Data_1[j].section == section)&& ((Math.abs($scope.baseAW1Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAW1Data_1[j].displacement)>10))){
                    $scope.disAW1[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAW1Data_2){
                if(($scope.baseAW1Data_2[j].section == section)&& ((Math.abs($scope.baseAW1Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAW1Data_2[j].displacement)>10))){
                    $scope.disAW1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW1Data_3){
                if(($scope.baseAW1Data_3[j].section == section)&& ((Math.abs($scope.baseAW1Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAW1Data_3[j].displacement)>10))){
                    $scope.disAW1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW1Data_4){
                if(($scope.baseAW1Data_4[j].section == section)&& ((Math.abs($scope.baseAW1Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAW1Data_4[j].displacement)>10))){
                    $scope.disAW1[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW1Data_5){
                if(($scope.baseAW1Data_5[j].section == section)&& ((Math.abs($scope.baseAW1Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAW1Data_5[j].displacement)>10))){
                    $scope.disAW1[i]=-100;
                    continue;
                }
            }
          
            
        }
        sectionLen = 0;
        for(i in $scope.disAW1){
            if($scope.disAW1[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAW1[i])
        }
        sectionLen=sectionLen+5;
        let data_AW1_1=new Array(sectionLen);
        let data_AW1_2=new Array(sectionLen);
        let data_AW1_3=new Array(sectionLen);
        let data_AW1_4=new Array(sectionLen);
        let data_AW1_5=new Array(sectionLen);

        for(i in $scope.disAW1){
            let section = $scope.disAW1[i];
            if(section!=-1){
                for(j in $scope.baseAW1Data_0){
                    if($scope.baseAW1Data_0[j].section==section)
                        data_AW1_1[section]=$scope.baseAW1Data_0[j].displacement;
                }
                for(j in $scope.baseAW1Data_1){
                    if($scope.baseAW1Data_1[j].section==section)
                        data_AW1_2[section]=$scope.baseAW1Data_1[j].displacement;
                }
                for(j in $scope.baseAW1Data_2){
                    if($scope.baseAW1Data_2[j].section==section)
                        data_AW1_3[section]=$scope.baseAW1Data_2[j].displacement;
                }
                for(j in $scope.baseAW1Data_3){
                    if($scope.baseAW1Data_3[j].section==section)
                        data_AW1_4[section]=$scope.baseAW1Data_3[j].displacement;
                }
                for(j in $scope.baseAW1Data_4){
                    if($scope.baseAW1Data_4[j].section==section)
                        data_AW1_5[section]=$scope.baseAW1Data_4[j].displacement;
                }
                $scope.showAW1 = true;
                $("#AW1_"+Math.floor(section/10)).css("fill","red");
                $("#AW1_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-圍牆線 1', 'AW1' , data_AW1_1,data_AW1_2,data_AW1_3,data_AW1_4,data_AW1_5,sectionLen);
        ////////////////AW2//////////////////
        $scope.disAW2 = [];
        for(i in $scope.baseAW2Data_0){
            if(Math.abs($scope.baseAW2Data_0[i].displacement)>0.5){
                $scope.disAW2.push($scope.baseAW2Data_0[i].section);
            }
        }
      
        for(i in $scope.disAW2){
            let section = $scope.disAW2[i];
            for(j in $scope.baseAW2Data_1){
                if(($scope.baseAW2Data_1[j].section == section)&& ((Math.abs($scope.baseAW2Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAW2Data_1[j].displacement)>10))){
                    $scope.disAW2[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAW2Data_2){
                if(($scope.baseAW2Data_2[j].section == section)&& ((Math.abs($scope.baseAW2Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAW2Data_2[j].displacement)>10))){
                    $scope.disAW2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW2Data_3){
                if(($scope.baseAW2Data_3[j].section == section)&& ((Math.abs($scope.baseAW2Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAW2Data_3[j].displacement)>10))){
                    $scope.disAW2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW2Data_4){
                if(($scope.baseAW2Data_4[j].section == section)&& ((Math.abs($scope.baseAW2Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAW2Data_4[j].displacement)>10))){
                    $scope.disAW2[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW2Data_5){
                if(($scope.baseAW2Data_5[j].section == section)&& ((Math.abs($scope.baseAW2Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAW2Data_5[j].displacement)>10))){
                    $scope.disAW2[i]=-100;
                    continue;
                }
            }
          
            
        }
        sectionLen = 0;
        for(i in $scope.disAW2){
            if($scope.disAW2[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAW2[i])
        }
        sectionLen=sectionLen+5;
        let data_AW2_1=new Array(sectionLen);
        let data_AW2_2=new Array(sectionLen);
        let data_AW2_3=new Array(sectionLen);
        let data_AW2_4=new Array(sectionLen);
        let data_AW2_5=new Array(sectionLen);

        for(i in $scope.disAW2){
            let section = $scope.disAW2[i];
            if(section!=-1){
                for(j in $scope.baseAW2Data_0){
                    if($scope.baseAW2Data_0[j].section==section)
                        data_AW2_1[section]=$scope.baseAW2Data_0[j].displacement;
                }
                for(j in $scope.baseAW2Data_1){
                    if($scope.baseAW2Data_1[j].section==section)
                        data_AW2_2[section]=$scope.baseAW2Data_1[j].displacement;
                }
                for(j in $scope.baseAW2Data_2){
                    if($scope.baseAW2Data_2[j].section==section)
                        data_AW2_3[section]=$scope.baseAW2Data_2[j].displacement;
                }
                for(j in $scope.baseAW2Data_3){
                    if($scope.baseAW2Data_3[j].section==section)
                        data_AW2_4[section]=$scope.baseAW2Data_3[j].displacement;
                }
                for(j in $scope.baseAW2Data_4){
                    if($scope.baseAW2Data_4[j].section==section)
                        data_AW2_5[section]=$scope.baseAW2Data_4[j].displacement;
                }
                $scope.showAW2 = true;
                $("#AW2_"+Math.floor(section/10)).css("fill","red");
                $("#AW2_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
            
                
        }
        
        $scope.plotChange('設備A-圍牆線 2', 'AW2' , data_AW2_1,data_AW2_2,data_AW2_3,data_AW2_4,data_AW2_5,sectionLen);
        ////////////////AW3//////////////////
        $scope.disAW3 = [];
        for(i in $scope.baseAW3Data_0){
            if(Math.abs($scope.baseAW3Data_0[i].displacement)>0.5){
                $scope.disAW3.push($scope.baseAW3Data_0[i].section);
            }
        }
      
        for(i in $scope.disAW3){
            let section = $scope.disAW3[i];
            for(j in $scope.baseAW3Data_1){
                if(($scope.baseAW3Data_1[j].section == section)&& ((Math.abs($scope.baseAW3Data_1[j].displacement)<0.01) || (Math.abs($scope.baseAW3Data_1[j].displacement)>10))){
                    $scope.disAW3[i]=-100;
                    continue;
                }
                
            }
            for(j in $scope.baseAW3Data_2){
                if(($scope.baseAW3Data_2[j].section == section)&& ((Math.abs($scope.baseAW3Data_2[j].displacement)<0.01) || (Math.abs($scope.baseAW3Data_2[j].displacement)>10))){
                    $scope.disAW3[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW3Data_3){
                if(($scope.baseAW3Data_3[j].section == section)&& ((Math.abs($scope.baseAW3Data_3[j].displacement)<0.01) || (Math.abs($scope.baseAW3Data_3[j].displacement)>10))){
                    $scope.disAW3[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW3Data_4){
                if(($scope.baseAW3Data_4[j].section == section)&& ((Math.abs($scope.baseAW3Data_4[j].displacement)<0.01) || (Math.abs($scope.baseAW3Data_4[j].displacement)>10))){
                    $scope.disAW3[i]=-100;
                    continue;
                }
            }
            for(j in $scope.baseAW3Data_5){
                if(($scope.baseAW3Data_5[j].section == section)&& ((Math.abs($scope.baseAW3Data_5[j].displacement)<0.01) || (Math.abs($scope.baseAW3Data_5[j].displacement)>10))){
                    $scope.disAW3[i]=-100;
                    continue;
                }
            }
          
            
        }
        sectionLen = 0;
        for(i in $scope.disAW3){
            if($scope.disAW3[i]!=-100)
                sectionLen = Math.max(sectionLen,$scope.disAW3[i])
        }
        sectionLen=sectionLen+5;
        let data_AW3_1=new Array(sectionLen);
        let data_AW3_2=new Array(sectionLen);
        let data_AW3_3=new Array(sectionLen);
        let data_AW3_4=new Array(sectionLen);
        let data_AW3_5=new Array(sectionLen);

        for(i in $scope.disAW3){
            let section = $scope.disAW3[i];
            if(section!=-1){
                for(j in $scope.baseAW3Data_0){
                    if($scope.baseAW3Data_0[j].section==section)
                        data_AW3_1[section]=$scope.baseAW3Data_0[j].displacement;
                }
                for(j in $scope.baseAW3Data_1){
                    if($scope.baseAW3Data_1[j].section==section)
                        data_AW3_2[section]=$scope.baseAW3Data_1[j].displacement;
                }
                for(j in $scope.baseAW3Data_2){
                    if($scope.baseAW3Data_2[j].section==section)
                        data_AW3_3[section]=$scope.baseAW3Data_2[j].displacement;
                }
                for(j in $scope.baseAW3Data_3){
                    if($scope.baseAW3Data_3[j].section==section)
                        data_AW3_4[section]=$scope.baseAW3Data_3[j].displacement;
                }
                for(j in $scope.baseAW3Data_4){
                    if($scope.baseAW3Data_4[j].section==section)
                        data_AW3_5[section]=$scope.baseAW3Data_4[j].displacement;
                }
                $scope.showAW3 = true;
                $("#AW3_"+Math.floor(section/10)).css("fill","red");
                $("#AW3_"+Math.floor(section/10)).css("fill-opacity",0.4);
            }
                
        }

        $scope.plotChange('設備A-檔土牆線 3', 'AW3' , data_AW3_1,data_AW3_2,data_AW3_3,data_AW3_4,data_AW3_5,sectionLen);
    }

    $scope.setHours = function () {
        for (let i = 0; i < 1; i++) {
            $scope.hours.push(i);
        }
    }
    $scope.getSearchDateFormat = function (d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }
    $scope.search = function () {
        if ($scope.selectedHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧小時")
        }
        if ($scope.selectedDate == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧日期")
        }

        for (i in $scope.selectedDate) {
            let d = $scope.selectedDate[i];
            $scope.getHomeBehaviorAH1($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAH2($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAH3($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAH4($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAH5($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAH6($scope.selectedHour, d, i);


            $scope.getHomeBehaviorAW1($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAW2($scope.selectedHour, d, i);
            $scope.getHomeBehaviorAW3($scope.selectedHour, d, i);
        }
        setTimeout(function () {
            $scope.findConstantChange();
            $scope.loading=false;
            $scope.$apply()
        }, 1000);
       

    }

    $scope.getLineData = function (lineDate) {
        let len = lineDate.length;
        if(len>0){
            let lastSection = lineDate[len - 1].section;
            let lineArray = new Array(lastSection);
            for (let i = 0; i < len; i++) {
                let section = lineDate[i].section;
                lineArray[section] = lineDate[i].displacement;
            }
            return lineArray;
        }else{
            return {};
        }   
    }

    $scope.getLabelData = function (length) {
        let labelArray = [];
        for (let i = 0; i < length; i++) {
            labelArray.push(i*10);
        }
        return labelArray;
    }
    $scope.plotChange = function (chartTitle, line,data1,data2,data3,data4,data5,sectionLen) {
        let optionsObj = {
            scales: {
                xAxes: [{ stacked: true,
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "區域線段（公分）",
                    }
                 }],

                yAxes: [{
                    stacked: false,
                    ticks: {
                        beginAtZero: false,
                        suggestedMax: 10
                    },
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "變位量（公分）",
                    }
                }]
            },
            onClick: function(evt, item) {
                if(item.length>0){
                    window.open("/section.html?section="+item[0]._model.label);
                }
            },
            title: {
                display: true,
                text: chartTitle
            }
        };
        var ctx ="";
        if (line == 'AH1')
            ctx = document.getElementById('changeAH1').getContext('2d');
        else if(line == 'AH2')
            ctx = document.getElementById('changeAH2').getContext('2d');
        else if(line == 'AH3')
            ctx = document.getElementById('changeAH3').getContext('2d');
        else if(line == 'AH4')
            ctx = document.getElementById('changeAH4').getContext('2d');
        else if(line == 'AH5')
            ctx = document.getElementById('changeAH5').getContext('2d');
        else if(line == 'AH6')
            ctx = document.getElementById('changeAH6').getContext('2d');
        else if(line == 'AW1')
            ctx = document.getElementById('changeAW1').getContext('2d');
        else if(line == 'AW2')
            ctx = document.getElementById('changeAW2').getContext('2d');
        else if(line == 'AW3')
            ctx = document.getElementById('changeAW3').getContext('2d');
       
        let chart1 = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: $scope.getLabelData(sectionLen),
                datasets: [
                    {
                        type: 'bar',
                        label: $scope.selectedDate[0],
                        data: data1,
                        backgroundColor: 'red',
                        borderWidth: 1
                    },
                    {
                        type: 'bar',
                        label: $scope.selectedDate[1],
                        data: data2,
                        backgroundColor: 'orange',
                        borderWidth: 1
                    },
                    {
                        type: 'bar',
                        label: $scope.selectedDate[2],
                        data: data3,
                        backgroundColor: 'yellow',
                        borderWidth: 1
                    },
                    {
                        type: 'bar',
                        label: $scope.selectedDate[3],
                        data: data4,
                        backgroundColor: 'green',
                        borderWidth: 1
                    },
                    {
                        type: 'bar',
                        label: $scope.selectedDate[4],
                        data: data5,
                        backgroundColor: 'blue',
                        borderWidth: 1
                    }
                ]
            },
            options: optionsObj
        });

    }

    $scope.init = function () {
        $scope.setHours();
        $scope.search();
    }
    $scope.init();
});

