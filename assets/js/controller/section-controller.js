
app.controller('sectionController', function ($scope, compareService) {
    $scope.chartAH1 = [];
    $scope.labelAH1 = [];
    $scope.chartAH2 = [];
    $scope.labelAH2 = [];
    $scope.chartAH3 = [];
    $scope.labelAH3 = [];
    $scope.chartAH4 = [];
    $scope.labelAH4 = [];
    $scope.chartAH5 = [];
    $scope.labelAH5 = [];
    $scope.chartAH6 = [];
    $scope.labelAH6 = [];

    $scope.chartAW1 = [];
    $scope.labelAW1 = [];
    $scope.chartAW2 = [];
    $scope.labelAW2 = [];
    $scope.chartAW3 = [];
    $scope.labelAW3 = [];

    $scope.plotChange = function (line) {
        let label = [];
        let chartData = [];
        let title = "";
        var ctx = "";
        if (line == 'AH1') {
            ctx = document.getElementById('changeAH1').getContext('2d');
            label = $scope.labelAH1;
            chartData = $scope.chartAH1;
            title = "設備A 房子線1";
        }
        else if (line == 'AH2') {
            ctx = document.getElementById('changeAH2').getContext('2d');
            label = $scope.labelAH2;
            chartData = $scope.chartAH2;
            title = "設備A 房子線2";
        }
        else if (line == 'AH3'){
            ctx = document.getElementById('changeAH3').getContext('2d');
            label = $scope.labelAH3;
            chartData = $scope.chartAH3;
            title = "設備A 房子線3";
        }
        else if (line == 'AH4'){
            ctx = document.getElementById('changeAH4').getContext('2d');
            label = $scope.labelAH4;
            chartData = $scope.chartAH4;
            title = "設備A 房子線4";
        }
        else if (line == 'AH5'){
            ctx = document.getElementById('changeAH5').getContext('2d');
            label = $scope.labelAH5;
            chartData = $scope.chartAH5;
            title = "設備A 房子線5";
        }
        else if (line == 'AH6'){
            ctx = document.getElementById('changeAH6').getContext('2d');
            label = $scope.labelAH6;
            chartData = $scope.chartAH6;
            title = "設備A 房子線6";
        }
        else if (line == 'AW1'){
            ctx = document.getElementById('changeAW1').getContext('2d');
            label = $scope.labelAW1;
            chartData = $scope.chartAW1;
            title = "設備A 防土牆1";
        }
        else if (line == 'AW2'){
            ctx = document.getElementById('changeAW2').getContext('2d');
            label = $scope.labelAW2;
            chartData = $scope.chartAW2;
            title = "設備A 防土牆2";
        }
        else if (line == 'AW3'){
            ctx = document.getElementById('changeAW3').getContext('2d');
            label = $scope.labelAW3;
            chartData = $scope.chartAW3;
            title = "設備A 防土牆3";
        }

        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: label,
                datasets: [
                    {
                        type: 'bar',
                        label: title,
                        data: chartData,
                        backgroundColor: 'red',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true,
                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "日期",
                        }
                    }],

                    yAxes: [{
                        stacked: false,
                        ticks: {
                            beginAtZero: false,
                            suggestedMax: 10
                        },
                        scaleLabel: {
                            display: true,
                            fontSize: 14,
                            labelString: "變位量（公分）",
                        }
                    }]
                },
                title: {
                    display: true
                }
            }
        });

    }



    $scope.getSection = function (section) {
        compareService.getSection(section, 0).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                let resData = data.data;
                for (i in resData) {
                    if (resData[i].name == 'AH1') {
                        $scope.chartAH1.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH1.push(resData[i].date);
                    } else if (resData[i].name == 'AH2') {
                        $scope.chartAH2.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH2.push(resData[i].date);
                    } else if (resData[i].name == 'AH3') {
                        $scope.chartAH3.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH3.push(resData[i].date);
                    } else if (resData[i].name == 'AH4') {
                        $scope.chartAH4.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH4.push(resData[i].date);
                    } else if (resData[i].name == 'AH5') {
                        $scope.chartAH5.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH5.push(resData[i].date);
                    } else if (resData[i].name == 'AH6') {
                        $scope.chartAH6.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAH6.push(resData[i].date);
                    } else if (resData[i].name == 'AW1') {
                        $scope.chartAW1.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAW1.push(resData[i].date);
                    } else if (resData[i].name == 'AW2') {
                        $scope.chartAW2.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAW2.push(resData[i].date);
                    } else if (resData[i].name == 'AW3') {
                        $scope.chartAW3.push(resData[i].displacement * resData[i].direction);
                        $scope.labelAW3.push(resData[i].date);
                    }
                }
                $scope.plotChange('AH1');
                $scope.plotChange('AH2');
                $scope.plotChange('AH3');
                $scope.plotChange('AH4');
                $scope.plotChange('AH5');
                $scope.plotChange('AH6');
                $scope.plotChange('AW1');
                $scope.plotChange('AW2');
                $scope.plotChange('AW3');
            }
        });
    }
    $scope.init = function () {
        $scope.section = getUrlParameter('section');
        $scope.getSection($scope.section / 10);
    }
    $scope.init();
});

