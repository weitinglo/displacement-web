
import * as THREE from '../../build/three.module.js';
import { TrackballControls } from '../../jsm/controls/TrackballControls.js';
import { OBJLoader2 } from '../../jsm/loaders/OBJLoader2.js';
import { MTLLoader } from '../../jsm/loaders/MTLLoader.js';
import { MtlObjBridge } from '../../jsm/loaders/obj2/bridge/MtlObjBridge.js';

app.controller('homeController', function ($scope, displacementService) {
    updateMenu('home');
    var camera, controls, scene, renderer;
    var DESCENDER_ADJUST = 1.28;
    var objId = 0;
    $scope.selectedHour = 0;
    $scope.selectedDate = null;
    $scope.hours = [];
    $scope.preLine = 0;
    //$scope.lineType = ['AH1', 'AH2', 'AH3', 'AH4', 'AH5', 'AH6', 'AH7', 'AH8', 'AW1', 'AW2', 'AW3', 'BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6', 'BH7', 'BH8', 'BH9', 'BW1', 'BW2'];
    $scope.lineType = ['AH1', 'AH2', 'AH3', 'AH4', 'AH5', 'AH6', 'AH7', 'AW1', 'AW2', 'AW3', 'BH1', 'BH2', 'BH3', 'BH4', 'BH5', 'BH6', 'BH7', 'BH8', 'BH9', 'BW1', 'BW2'];

    $scope.compareData = {};
    $scope.compareData['AH1'] = { data: [], name: "設備A-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartAH1", showChart: false };
    $scope.compareData['AH2'] = { data: [], name: "設備A-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartAH2", showChart: false };
    $scope.compareData['AH3'] = { data: [], name: "設備A-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartAH3", showChart: false };
    $scope.compareData['AH4'] = { data: [], name: "設備A-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartAH4", showChart: false };
    $scope.compareData['AH5'] = { data: [], name: "設備A-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartAH5", showChart: false };
    $scope.compareData['AH6'] = { data: [], name: "設備A-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartAH6", showChart: false };
    $scope.compareData['AH7'] = { data: [], name: "設備A-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartAH7", showChart: false };
    $scope.compareData['AH8'] = { data: [], name: "設備A-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartAH8", showChart: false };
    $scope.compareData['AW1'] = { data: [], name: "設備A-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartAW1", showChart: false };
    $scope.compareData['AW2'] = { data: [], name: "設備A-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartAW2", showChart: false };
    $scope.compareData['AW3'] = { data: [], name: "設備A-圍牆線 3", chart: "顯示圍牆線 3 表格", chartId: "chartAW3", showChart: false };

    $scope.compareData['BH1'] = { data: [], name: "設備B-房子線 1", chart: "顯示房子線 1 表格", chartId: "chartBH1", showChart: false };
    $scope.compareData['BH2'] = { data: [], name: "設備B-房子線 2", chart: "顯示房子線 2 表格", chartId: "chartBH2", showChart: false };
    $scope.compareData['BH3'] = { data: [], name: "設備B-房子線 3", chart: "顯示房子線 3 表格", chartId: "chartBH3", showChart: false };
    $scope.compareData['BH4'] = { data: [], name: "設備B-房子線 4", chart: "顯示房子線 4 表格", chartId: "chartBH4", showChart: false };
    $scope.compareData['BH5'] = { data: [], name: "設備B-房子線 5", chart: "顯示房子線 5 表格", chartId: "chartBH5", showChart: false };
    $scope.compareData['BH6'] = { data: [], name: "設備B-房子線 6", chart: "顯示房子線 6 表格", chartId: "chartBH6", showChart: false };
    $scope.compareData['BH7'] = { data: [], name: "設備B-房子線 7", chart: "顯示房子線 7 表格", chartId: "chartBH7", showChart: false };
    $scope.compareData['BH8'] = { data: [], name: "設備B-房子線 8", chart: "顯示房子線 8 表格", chartId: "chartBH8", showChart: false };
    $scope.compareData['BH9'] = { data: [], name: "設備B-房子線 9", chart: "顯示房子線 9 表格", chartId: "chartBH9", showChart: false };
    $scope.compareData['BW1'] = { data: [], name: "設備B-圍牆線 1", chart: "顯示房子線 1 表格", chartId: "chartBW1", showChart: false };
    $scope.compareData['BW2'] = { data: [], name: "設備B-圍牆線 2", chart: "顯示圍牆線 2 表格", chartId: "chartBW2", showChart: false };

    $scope.maxMin = {};
    for (let i in $scope.lineType) {
        $scope.maxMin[$scope.lineType[i]] = { max: "", min: "" };
    }


    var chartAW1 = null;
    var chartAW2 = null;
    var chartAW3 = null;
    var chartAH1 = null;
    var chartAH2 = null;
    var chartAH3 = null;
    var chartAH4 = null;
    var chartAH5 = null;
    var chartAH6 = null;
    var chartAH7 = null;
    var chartAH8 = null;

    var chartBW1 = null;
    var chartBW2 = null;
    var chartBH1 = null;
    var chartBH2 = null;
    var chartBH3 = null;
    var chartBH4 = null;
    var chartBH5 = null;
    var chartBH6 = null;
    var chartBH7 = null;
    var chartBH8 = null;
    var chartBH9 = null;

    var filter = false;




    $scope.getCompare = function (hour, date, line) {
        displacementService.getCompare(line, hour, date).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                drawCoords(data.data);
                $scope.findMaxMin(data.data, line);
                $scope.compareData[line].data = data.data;
                if (filter == true) {
                    for (i in data.data) {
                        if (Math.abs(data.data[i].displacement) > 3) {
                            displacementService.saveFilter(data.data[i].name, data.data[i].section, 0.1, 0);
                        }
                    }
                }
            }
        });
    }

    $scope.findMaxMin = function (data, type) {
        let min = 100;
        let max = -100;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].displacement;
            if (value > max) {
                max = value;
            } else if (value < min) {
                min = value;
            }
        }
        $scope.maxMin[type].max = max;
        $scope.maxMin[type].min = min;

    }
    $scope.setHours = function () {
        for (let i = 0; i < 1; i++) {
            $scope.hours.push(i);
        }
    }
    $scope.getSearchDateFormat = function (d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }
    $scope.search = function () {
        if ($scope.selectedHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧小時")
        }
        if ($scope.selectedDate == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧日期")
        }
        //   removeEntity('current_mesh');
        objId = 0;
        for (let i in $scope.lineType) {
            $scope.getCompare($scope.selectedHour, $scope.getSearchDateFormat($scope.selectedDate), $scope.lineType[i]);

        }
    }

    $scope.plotAW = function (chartTitle, line) {
        $scope.preLine = line;
        let chartData = [];
        var ctx = "";
        chartData = $scope.compareData[line].data;
        ctx = document.getElementById($scope.compareData[line].chartId).getContext('2d');
        $scope.compareData[line].showChart = !$scope.compareData[line].showChart;
        $scope.chartData = [];
        $scope.label = [];
        let len = chartData.length;
        let lastSection = chartData[len - 1].section
        $scope.chartData = new Array(lastSection);
        for (let i = 0; i < len; i++) {
            let section = chartData[i].section;
            $scope.chartData[section] = chartData[i].displacement;
        }
        for (let i = 0; i < lastSection; i++) {
            $scope.label.push(i * 10);
        }
        let options = {
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "區域線段（公分）",
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "變位量（公分）",
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label = '設備A-房子線 1') {
                            console.log(tooltipItem)
                        }
                        console.log(data.datasets[tooltipItem.datasetIndex])
                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        return label;
                    }
                }
            },
            onClick: function (evt, item) {
                console.log(item)
                if (item.length > 0) {
                    window.open("/section.html?section=" + item[0]._model.label);
                }
            },
        }
        let chartDataSetting = {
            labels: $scope.label, datasets: [{ label: chartTitle, data: $scope.chartData, backgroundColor: 'red', borderWidth: 1 }]
        }
        if (line == 'AH1') {
            chartAH1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH1.update();
        } else if (line == 'AH2') {
            chartAH2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH2.update();
        } else if (line == 'AH3') {
            chartAH3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH3.update();
        } else if (line == 'AH4') {
            chartAH4 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH4.update();
        } else if (line == 'AH5') {
            chartAH5 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH5.update();
        } else if (line == 'AH6') {
            chartAH6 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH6.update();
        } else if (line == 'AH7') {
            chartAH7 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH7.update();
        } else if (line == 'AH8') {
            chartAH8 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAH8.update();
        } else if (line == 'AW1') {
            chartAW1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW1.update();
        } else if (line == 'AW2') {
            chartAW2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW2.update();
        } else if (line == 'AW3') {
            chartAW3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartAW3.update();
        } else if (line == 'BH1') {
            chartBH1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH1.update();
        } else if (line == 'BH2') {
            chartBH2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH2.update();
        } else if (line == 'BH3') {
            chartBH3 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH3.update();
        } else if (line == 'BH4') {
            chartBH4 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH4.update();
        } else if (line == 'BH5') {
            chartBH5 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH5.update();
        } else if (line == 'BH6') {
            chartBH6 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH6.update();
        } else if (line == 'BH7') {
            chartBH7 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH7.update();
        } else if (line == 'BH8') {
            chartBH8 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH8.update();
        } else if (line == 'BH9') {
            chartBH9 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBH9.update();
        } else if (line == 'BW1') {
            chartBW1 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBW1.update();
        } else if (line == 'BW2') {
            chartBW2 = new Chart(ctx, { type: 'bar', data: chartDataSetting, options: options });
            chartBW2.update();
        }
    }

    /*Three JS*/
    function get_gradient_color(coord_z) {
        if (coord_z > 1 & coord_z <= 3) {
            return [0, 1, 0];

        } else if (coord_z > 3 & coord_z <= 5) {
            return [1, 1, 0];
        } else if (coord_z > 5) {
            return [1, 0, 0];
        } else {
            return [0, 0, 0];
        }
    }
    function getCanvasColor(color) {
        return "rgba(" + color.r + "," + color.g + "," + color.b + "," + color.a + ")";
    }
    function roundRect(ctx, x, y, w, h, r, borderThickness, borderColor, fillColor) {
        // no point in drawing it if it isn't going to be rendered 
        if (fillColor == undefined && borderColor == undefined)
            return;

        x -= borderThickness + r;
        y += borderThickness + r;
        w += borderThickness * 2 + r * 2;
        h += borderThickness * 2 + r * 2;

        ctx.beginPath();
        ctx.moveTo(x + r, y);
        ctx.lineTo(x + w - r, y);
        ctx.quadraticCurveTo(x + w, y, x + w, y - r);
        ctx.lineTo(x + w, y - h + r);
        ctx.quadraticCurveTo(x + w, y - h, x + w - r, y - h);
        ctx.lineTo(x + r, y - h);
        ctx.quadraticCurveTo(x, y - h, x, y - h + r);
        ctx.lineTo(x, y - r);
        ctx.quadraticCurveTo(x, y, x + r, y);
        ctx.closePath();

        ctx.lineWidth = borderThickness;

        // background color 
        // border color 

        // if the fill color is defined, then fill it 
        if (fillColor != undefined) {
            ctx.fillStyle = getCanvasColor(fillColor);
            ctx.fill();
        }

        if (borderThickness > 0 && borderColor != undefined) {
            ctx.strokeStyle = getCanvasColor(borderColor);
            ctx.stroke();
        }
    }
    function makeTextSprite(message, x, y, z, parameters) {
        if (parameters === undefined) parameters = {};

        var fontface = parameters.hasOwnProperty("fontface") ?
            parameters["fontface"] : "Arial";

        var fontsize = parameters.hasOwnProperty("fontsize") ?
            parameters["fontsize"] : 18;

        var borderThickness = parameters.hasOwnProperty("borderThickness") ?
            parameters["borderThickness"] : 4;

        var borderColor = parameters.hasOwnProperty("borderColor") ?
            parameters["borderColor"] : { r: 0, g: 0, b: 0, a: 1.0 };

        var fillColor = parameters.hasOwnProperty("fillColor") ?
            parameters["fillColor"] : undefined;

        var textColor = parameters.hasOwnProperty("textColor") ?
            parameters["textColor"] : { r: 0, g: 0, b: 0, a: 1.0 };

        var radius = parameters.hasOwnProperty("radius") ?
            parameters["radius"] : 6;

        var vAlign = parameters.hasOwnProperty("vAlign") ?
            parameters["vAlign"] : "center";

        var hAlign = parameters.hasOwnProperty("hAlign") ?
            parameters["hAlign"] : "center";

        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');

        context.font = fontsize + "px " + fontface;
        context.textBaseline = "alphabetic";
        context.textAlign = "left";

        // get size data (height depends only on font size) 
        var metrics = context.measureText(message);
        var textWidth = metrics.width;


        // find the center of the canvas and the half of the font width and height 
        // we do it this way because the sprite's position is the CENTER of the sprite 
        var cx = canvas.width / 2;
        var cy = canvas.height / 2;
        var tx = textWidth / 2.0;
        var ty = fontsize / 2.0;

        // then adjust for the justification 
        if (vAlign == "bottom")
            ty = 0;
        else if (vAlign == "top")
            ty = fontsize;

        if (hAlign == "left")
            tx = textWidth;
        else if (hAlign == "right")
            tx = 0;

        // the DESCENDER_ADJUST is extra height factor for text below baseline: g,j,p,q. since we don't know the true bbox 
        roundRect(context, cx - tx, cy + ty + 0.28 * fontsize,
            textWidth, fontsize * DESCENDER_ADJUST, radius, borderThickness, borderColor, fillColor);

        // text color.  Note that we have to do this AFTER the round-rect as it also uses the "fillstyle" of the canvas 
        context.fillStyle = getCanvasColor(textColor);

        context.fillText(message, cx - tx, cy + ty);



        // canvas contents will be used for a texture 
        var texture = new THREE.Texture(canvas)
        texture.needsUpdate = true;

        var spriteMaterial = new THREE.SpriteMaterial({ map: texture });
        var sprite = new THREE.Sprite(spriteMaterial);

        // we MUST set the scale to 2:1.  The canvas is already at a 2:1 scale, 
        // but the sprite itself is square: 1.0 by 1.0 
        // Note also that the size of the scale factors controls the actual size of the text-label 
        sprite.scale.set(4, 2, 1);

        // set the sprite's position.  Note that this position is in the CENTER of the sprite 
        sprite.position.set(x, y, z);

        return sprite;
    }


    function setupPlanes() {
        $scope.positions = [];
        let pos1 = {
            xPos: -2.2,
            yPos: -14.4,
            zPos: -0.06,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos1);
        let pos2 = {
            xPos: -1.4,
            yPos: -14.8,
            zPos: -0.05,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos2);

        let pos3 = {
            xPos: -0.55,
            yPos: -15.25,
            zPos: -0.04,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos3);

        let pos4 = {
            xPos: 0.3 ,
            yPos: -15.7,
            zPos: -0.03,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos4);

        let pos5 = {
            xPos: 1.15 ,
            yPos: -16.2,
            zPos: -0.02,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos5);

        let pos6 = {
            xPos: 2 ,
            yPos: -16.7,
            zPos: -0.01,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos6);

        let pos7= {
            xPos: 2.8 ,
            yPos: -17.2,
            zPos: -0,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos7);

        let pos8= {
            xPos: 3.65 ,
            yPos: -17.65,
            zPos: 0.01,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos8);

        let pos9= {
            xPos: 4.5,
            yPos: -18.1,
            zPos: 0.02,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos9);

        let pos10= {
            xPos: 5.4,
            yPos: -18.5,
            zPos: 0.03,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos10);

        let pos11= {
            xPos: 6.258,
            yPos: -19.01,
            zPos: 0.04,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos11);

        let pos12= {
            xPos: 7.12,
            yPos: -19.5,
            zPos: 0.05,
            xRot: 1.5,
            yRot: -0.45
        }
        $scope.positions.push(pos12);

        let pos13= {
            xPos: 7.9,
            yPos: -20,
            zPos: 0.06,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos13);

        let pos14= {
            xPos: 8.825,
            yPos: -20.4,
            zPos: 0.07,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos14);

        let pos15= {
            xPos: 9.65,
            yPos: -20.9,
            zPos: 0.08,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos15);

        let pos16= {
            xPos: 10.45,
            yPos: -21.3,
            zPos: 0.09,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos16);

        let pos17= {
            xPos: 11.3,
            yPos: -21.8,
            zPos: 0.10,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos17);

        let pos18= {
            xPos: 12.19,
            yPos: -22.25,
            zPos: 0.11,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos18);
        
        let pos19= {
            xPos: 13.05,
            yPos: -22.7,
            zPos: 0.16,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos19);

        let pos20= {
            xPos: 13.9,
            yPos: -23.2,
            zPos: 0.18,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos20);

        let pos21= {
            xPos: 14.75,
            yPos: -23.7,
            zPos: 0.21,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos21);

        let pos22= {
            xPos: 15.61,
            yPos: -24.2,
            zPos: 0.27,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos22);

        let pos23= {
            xPos: 16.45,
            yPos: -24.7,
            zPos: 0.3,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos23);

        let pos24= {
            xPos: 17.35,
            yPos: -25.1,
            zPos: 0.33,
            xRot: 1.5,
            yRot: -0.48
        }
        $scope.positions.push(pos24);


        for (let i in $scope.positions) {
            console.log($scope.positions[i]);
            addPlanes($scope.positions[i])
            let index = parseInt(i) + 1;
            let txSprite = makeTextSprite("區段" + index, $scope.positions[i].xPos+0.1, $scope.positions[i].yPos + 0.5, $scope.positions[i].zPos, { fontsize: 14, fontface: "Arial", borderColor: { r: 0, g: 255, b: 0, a: 1.0 }, borderThickness: 0, radius: 8, fillColor: { r: 255, g: 255, b: 255, a: 0.8 }, vAlign: "center", hAlign: "left" });
            scene.add(txSprite);
        }



    }


    function addPlanes(jsonVal) {
        const geometry = new THREE.PlaneGeometry(1, 1, 1);
        const material = new THREE.MeshBasicMaterial({ color: new THREE.Color("rgb(255, 0, 0)"), side: THREE.DoubleSide });
        const plane = new THREE.Mesh(geometry, material);
        plane.position.x = jsonVal.xPos;
        plane.position.y = jsonVal.yPos;
        plane.position.z = jsonVal.zPos;
        plane.rotation.y = jsonVal.yRot;
        plane.rotation.x = jsonVal.xRot;
        scene.add(plane);
    }


    function drawCoords(coords_array) {
        var position = [];
        var color = [];

        for (var i = 0; i < coords_array.length; i++) {
            position.push(parseFloat(coords_array[i].base_x));
            position.push(parseFloat(coords_array[i].base_y));
            position.push(parseFloat(coords_array[i].base_z));

            const coord_color = get_gradient_color(coords_array[i].displacement);
            color.push(coord_color[0], coord_color[1], coord_color[2]);
        }




        // build geometry
        var geometry = new THREE.BufferGeometry();
        if (position.length > 0) geometry.setAttribute('position', new THREE.Float32BufferAttribute(position, 3));
        if (color.length > 0) geometry.setAttribute('color', new THREE.Float32BufferAttribute(color, 3));
        geometry.computeBoundingSphere();

        // build material
        var material = new THREE.PointsMaterial({ size: 2 });
        if (color.length > 0) {
            material.vertexColors = true;
        } else {
            material.color.setHex(Math.random() * 0xffffff);
        }

        // build point cloud

        var mesh = new THREE.Points(geometry, material);
        mesh.name = 'current_mesh' + objId;
        objId++;
        scene.add(mesh);
        const center = mesh.geometry.boundingSphere.center;
        controls.target.set(center.x, center.y, center.z);
        controls.update();
        render();
    }
    function animate() {
        requestAnimationFrame(animate);
        controls.update();
    }

    function render() {
        renderer.render(scene, camera);
    }
    function draw_house_model(model_name) {
        var mtlLoader = new MTLLoader();
        mtlLoader.load('../chiper_model/' + model_name + '.mtl', (mtlParseResult) => {
            const materials = MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);
            var objLoader = new OBJLoader2();
            objLoader.addMaterials(materials);

            objLoader.load('../chiper_model/' + model_name + '.obj', (root) => {
                scene.add(root);
                root.position.set(0, -0.1, -0.15);
                render();
            });
        });
    }
    function put_instruction() {
        var text2 = document.createElement('div');
        text2.style.position = 'absolute';
        text2.style.width = 100;
        text2.style.height = 100;
        text2.style.color = "white";
        text2.innerHTML = "【操作說明】<br>滑鼠左鍵：拖曳<br>滑鼠滾輪：前後移動";
        text2.style.top = 30 + 'px';
        text2.style.left = 35 + 'px';
        document.getElementById('home_canvas').appendChild(text2);
    }
    $scope.threeJSinit = function () {
        const menu_WIDTH = 300;
        const HEIGHT = 900;

        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x000000);

        camera = new THREE.PerspectiveCamera(20, (window.innerWidth - menu_WIDTH) / HEIGHT, 0.01, 9000);
        camera.position.x = 100;
        camera.position.y = -50;
        camera.position.z = 30;
        camera.up.set(10, 1, 100);
        scene.add(camera);

        var light = new THREE.DirectionalLight(0xFFFFFF, 1.5, 500); // white, intensity=1, distance = 500
        light.position.set(10, 10, 25); // (x, y, z)
        scene.add(light);
        const axesHelper = new THREE.AxesHelper(5);
        scene.add(axesHelper);

        var models = ['Tile_+002_+002_+000', 'Tile_+003_+001_+000', 'Tile_+003_+002_+000']
        models.forEach(element => draw_house_model(element));
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor("#e5e5e5");
        renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);

        const the_canvas = document.getElementById('home_canvas');
        the_canvas.appendChild(renderer.domElement);
        put_instruction();

        window.addEventListener('resize', () => {
            renderer.setSize(window.innerWidth - menu_WIDTH, HEIGHT);
            camera.aspect = (window.innerWidth - menu_WIDTH) / HEIGHT;

            camera.updateProjectionMatrix();
        })

        controls = new TrackballControls(camera, the_canvas);
        controls.rotateSpeed = 2;
        controls.zoomSpeed = 5.0;
        controls.noZoom = false;
        controls.staticMoving = true;
        controls.dynamicDampingFactor = 0.3;
        render();
        controls.addEventListener('change', render);
        controls.update();
    }
    $scope.init = function () {
        // $scope.setHours();
        // $scope.selectedDate = new Date('2020-10-12');
        // $scope.search();
        $scope.threeJSinit();
        animate();
        setupPlanes()

    }
    $scope.init();
});

